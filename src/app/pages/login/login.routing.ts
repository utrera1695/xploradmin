import { Routes, RouterModule }  from '@angular/router';

import { Login } from './login.component';
import { ModuleWithProviders } from '@angular/core';
import {LoginGuard} from '../../guards/index';

// noinspection TypeScriptValidateTypes
export const routes: Routes = [
  {
    path: '',
    component: Login,
    canActivate: [LoginGuard] 
  }
];

export const routing: ModuleWithProviders = RouterModule.forChild(routes);
