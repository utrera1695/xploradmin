import {Component} from '@angular/core';
import {FormGroup, AbstractControl, FormBuilder, Validators} from '@angular/forms';
import { Admin } from '../../models/admin'
import {AdminService} from '../../services/admin.services'
import {MailService} from '../../services/mail.services'

@Component({
  selector: 'login',
  templateUrl: './login.html',
  styleUrls: ['./login.scss']
})
export class Login {
  public admin: Admin;
  public  identity;
  public token;

public show: boolean;
  public form:FormGroup;
  public message: string;
  public messageE: string;
  constructor(
    
    private _adminService:AdminService,
    private _mailService: MailService
  ){
    
    this.admin= new Admin('','','','','','','','');
    this.message= '';
    this.show=false;
  }

  public onSubmit(){
    if((this.admin.username!='')&&(this.admin.password!='')){
    this._adminService.signup(this.admin).subscribe(
       response =>{
         

         if(!response.user){
           //alert("No se pudo identificar correctamente al usuario")
           this.message='Usuario o contraseña no válido'
         }else{
           let identity = response.user;
         this.identity = identity;
           //crear elemento en el localStorage
           localStorage.setItem('identity',JSON.stringify(identity));
           //conseguir el token para enviarlo a las peticiones http
           this._adminService.signup(this.admin, 'true').subscribe(
             response =>{
               let token = response.token;
               this.token = token;

               if(this.token.length <=0){
                 alert("No se ha generado el token")
               }else{
                 //crear elemento en el localStorage para tener el token disponible
                 localStorage.setItem('token',token);
                 //conseguir el token para enviarlo a las peticiones http
                 let win = (window as any);
                  win.location.reload();
               }
             },
             error =>{
               var errorMessage = <any>error;
               if(errorMessage != null){
                 console.log(error);
               }
             }
          );  
         }
       },
       error =>{
         var errorMessage = <any>error;
         if(errorMessage != null){
           console.log(error);
         }
       }
    );
  }else{
    this.message='Debe rellenar todos los campos para poder ingresar';
  }
}
public newPass='';
makeid(){
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 7; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

   this.newPass=text;
  }
recoveryPassword(){
  if((this.admin.email.search('@')>4)){
    this._adminService.getAdminEmail(this.admin.email).subscribe(
      response=>{
        if(!response.admin){
          this.messageE='Este correo electrónico no se encuentra en nuestra base de datos'
        }else{
          let admin = response.admin 
          this.makeid()
          admin.pass=this.newPass;
          admin.password=this.newPass;
          this._adminService.update(admin).subscribe(
            response=>{
              if(response.adminU){
                this._mailService.sendMail(admin).subscribe(
                  response=>{
                    if(response.info){
                      this.messageE="Hemos enviado a su cuenta de correo electrónico su nueva contraseña temporal";
                    }
                  }
                )
              }
            }
          )
        }
      },
      error=>{

      }
    )
  }else{
    this.messageE='Debe especificar un correo electrónico valido '+ this.admin.email.search('@') +' '+this.admin.email.search('.');
  }
}

}
