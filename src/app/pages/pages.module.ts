import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';

import { routing }       from './pages.routing';
import { NgaModule } from '../theme/nga.module';
import { AppTranslationModule } from '../app.translation.module';
import {AuthGuard} from '../guards/index';
import { Pages } from './pages.component';
import {EmpresaService} from '../services/empresa.services'

@NgModule({
  imports: [CommonModule, AppTranslationModule, NgaModule, routing],
  declarations: [Pages],
  providers: [
  	AuthGuard,
  	EmpresaService
  	]
})
export class PagesModule {
}
