import { Routes, RouterModule }  from '@angular/router';

import { Components } from './components.component';
import { TreeView } from './components/treeView/treeView.component';
import { ConsultarC } from './components/consultarC/consultarC.component';
import {AuthGuard} from '../../guards/index';
// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: Components,
    children: [
      { path: 'treeview', component: TreeView },
      { path: 'consultarC', component: ConsultarC}
    
    ],
    canActivate: [AuthGuard]
  }
];

export const routing = RouterModule.forChild(routes);
