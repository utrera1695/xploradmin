import {Component, OnInit} from '@angular/core';
import {DragulaService} from 'ng2-dragula/ng2-dragula'
import {FormGroup, AbstractControl, FormBuilder, Validators} from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DefaultModal2 } from './default-modal/default-modal.component';

import {Cuestionario} from '../../../../models/cuestionario'
import {CuestionarioService} from '../../../../services/cuestionario.services'
import {Pregunta} from '../../../../models/pregunta';
import {PreguntaService} from '../../../../services/pregunta.services'
import {Opciones} from '../../../../models/opciones';
import {OpcionesService} from '../../../../services/opciones.services'

@Component({
  selector: 'tree-view',
  templateUrl: './treeView.html',
  styleUrls:['./style.scss']
})

export class TreeView implements OnInit {
  
  public cuestionario :Cuestionario;
  public cuestionarioa : boolean;
  public pregunta: Pregunta;
  public preguntas: Pregunta[];
  public opciones: Opciones;
  public deletep:boolean;
  public deleteshow:boolean;
  public restP: number;
  public p:number;
  public options = [
  	{value: 'si y no', display:'Si y No'},
  	{value: 'opción simple', display:'Opción Simple'},
  	{value: 'opción múltiple', display:'Opción Múltiple'},
  	{value: 'respuesta libre', display:'Respuesta Libre'},
  	{value: 'numérica', display:'Numérica'},
  ];
  constructor(
  	private cuestionarioService: CuestionarioService,
  	private preguntaService: PreguntaService,
  	private dragulaService: DragulaService,
  	private modalService: NgbModal,
  	private opcionesService: OpcionesService
  	
  	){
  		this.restP=this.p=82;
    
  		this.deleteshow=false;
  		this.deletep=false;
	  	this.cuestionarioa = false;
  		this.cuestionario = new Cuestionario('','','',JSON.parse(localStorage.getItem('identity')).empresa);
	  	this.pregunta = new Pregunta('','','','');
	  	dragulaService.drag.subscribe((value)=>{
	  		this.onDrag(value.slice(1));
	  	});
	  	dragulaService.drop.subscribe((value)=>{
	  		this.onDrop(value.slice(1));
	  	});
	  	this.preguntas = new Array<Pregunta>();
	  	this.opciones = new Opciones('','','','','','')
	}
	ngOnInit(){
		this.cuestionario = new Cuestionario('','','',JSON.parse(localStorage.getItem('identity')).empresa);
	}

	lgModalShow(pregunta) {
    	const activeModal = this.modalService.open(DefaultModal2, {size: 'lg'});activeModal.componentInstance.pregunta = pregunta;
    	activeModal.componentInstance.modalHeader = pregunta.nombre;	
  	}

  	frasep(){
    	this.p=this.restP-this.pregunta.nombre.length;
  	}
  	public errorC: string;
	crearCuestionario(){
		//console.log(this.cuestionario);
		if(this.cuestionario.nombreC != ''){
			this.cuestionarioService.save(this.cuestionario).subscribe(
	        response =>{
	          
	          if(!response.cuestionario){
	            //mensaje de error
	            alert('Error al registrarse');
	          }else{
	          	
	          	this.cuestionario = response.cuestionario;
	           	this.cuestionarioa = true;

	          }
	        },
	        error=>{
	          var errorMessage = <any>error;
	          if(errorMessage != null){
	            //this.errorMessage = body.message;
	            console.log(error);
	       	  }
	        }
	      );
		}else{
			this.errorC ='Debe darle un nombre al cuestionario';
		}
	}
	public errorP: string;
	crearPregunta(){
		if(this.cuestionario._id != ''){
			if(this.preguntas.length<5){
				if((this.pregunta.nombre != '')&&(this.pregunta.tipo != '')){
					this.errorP = '';
				this.pregunta.cuestionario = this.cuestionario._id;
				if((this.pregunta.tipo == 'opción simple')||(this.pregunta.tipo == 'opción múltiple')||(this.pregunta.tipo == 'numérica')){
					if(((this.opciones.opcion1!='')&&(this.opciones.opcion2!=''))||((this.opciones.opcion3!='')||(this.opciones.opcion4!=''))){
					if((this.pregunta.tipo == 'numérica')&&(this.opciones.opcion1>=this.opciones.opcion2)){		
						this.errorP='El rango mínimo no puede ser mayor  o igual al rango máximo'
					}else{

						this.preguntaService.save(this.pregunta).subscribe(
							response =>{
								let preg = response.pregunta;
								if(!preg._id){
									alert('Error al crear pregunta');
								}else{
									this.pregunta = preg;
									this.preguntas.push(preg);
									this.pregunta = new Pregunta('','','','');
									this.deletep=false;
									this.id='';
									this.deleteshow=false;
										this.opciones.pregunta= preg._id;
										console.log(this.opciones);
										this.opcionesService.save(this.opciones).subscribe(
											response =>{
												if(!response.opciones){
													alert('no se pudo crear las opciones');
												}
												else{
													console.log(response);
													this.opciones = new Opciones('','','','','','')
												}
											},
											error =>{
												var errorMessage = <any>error;
								          		if(errorMessage != null){
								            		//this.errorMessage = body.message;
								            		console.log(error);
								       	  		}
											}
										)
								}
							},
							error =>{
								var errorMessage = <any>error;
				          		if(errorMessage != null){
				            		//this.errorMessage = body.message;
				            		console.log(error);
				       	  		}
							}
						)
					}
				}else{
					this.errorP= 'Debe rellenar todos los campos antes de guardar'	
				
				}
			}else{
				this.preguntaService.save(this.pregunta).subscribe(
					response =>{
						let preg = response.pregunta;
						if(!preg._id){
							alert('Error al crear pregunta');
						}else{
							this.pregunta = preg;
							this.preguntas.push(preg);
							this.pregunta = new Pregunta('','','','');
						}
					},
					error=>{

					}
				)
			}

				}else{
					this.errorP= 'Debe rellenar todos los campos antes de guardar'	
				}
			}else{
				this.errorP= 'Solo se permiten 5 preguntas por cuestionario'
			}
		}else{
			alert('Debe darle nombre a un cuestionario primero');
		}
	}


  	public idc: string;
  	public deleteC: boolean;
  	public deleteCshow: boolean;
  	delete(cuestionarioId){
		this.idc=''
		if(this.deleteC){
			this.cuestionarioService.delete(cuestionarioId).subscribe(
				response =>{
					if(!response.cuestionario){
						alert('Error al eliminar cuestionario');
					}else{
						this.cuestionarioa = false;		
					}
				},
				error =>{
					var errorMessage = <any>error;
		          	if(errorMessage != null){
		            	//this.errorMessage = body.message;
		            	console.log(error);
		       		}
				}
			);
		}
	}
	listPreguntas(){
	    this.preguntaService.listPreguntas(this.cuestionario._id).subscribe(
	        response =>{
	          this.preguntas=response.pregunta;
	          if(!response.pregunta){
	          }else{
	            this.preguntas = response.pregunta;
	          }
	            
	          },
	          error=>{
	            var errorMessage = <any>error;
	            if(errorMessage != null){
	              //this.errorMessage = body.message;
	              console.log(error);
	            }
	        }
	    );

	}
	
	validateP(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode( key );
    var regex = /[0-9]/;
    if( !regex.test(key) ) {
      theEvent.returnValue = false;
      if(theEvent.preventDefault) theEvent.preventDefault();
    }
  }
	public id: string;
	borrarp(preguntaId){
		this.id=preguntaId;
		if(this.deletep){
			this.preguntaService.delete(preguntaId).subscribe(
				response =>{
					if(!response.pregunta){
						alert('Error al eliminar la pregunra');
					}else{
						this.deletep=false;
						this.id='';
						this.deleteshow=false;
						this.listPreguntas();
					}
				},
				error =>{
					var errorMessage = <any>error;
		          	if(errorMessage != null){
		            	//this.errorMessage = body.message;
		            	console.log(error);
		       		}
				}
			)
		}
	}


	private hasClass (el:any, name: string){
		console.log('hasClass:',el,name);
		return new RegExp('(?:^|\\s+)'+name+'(?:\\s+|$)').test(el.className);
	}

	private addClass (el: any, name: string){
		if (!this.hasClass(el, name)){
			el.className = el.ckassName ? [el.className,name].join(''):name;
			console.log('addClass:'+el,name);
		}
	}

	private removeClass (el: any, name: string){
		if(this.hasClass(el,name)){
			el.className=el.className.replace(new RegExp('(?:^|\\s+)'+name+'(?:\\s+|$)','g'),'');
			console.log('removeClass:',el,name);
		}
	}

	private onDrag (args){
		let [e, el] = args;
		this.removeClass(e,'ex-moved');
		console.log('onDrag',e);
	}

	private onDrop (args){
		let [e, el] = args;
		this.addClass(e, 'ex-moved');
		console.log('onDrop',e);
	}

}