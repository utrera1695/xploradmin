import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {Pregunta} from '../../../../../models/pregunta';
import {PreguntaService} from '../../../../../services/pregunta.services'
import {Opciones} from '../../../../../models/opciones';
import {OpcionesService} from '../../../../../services/opciones.services'

@Component({
  selector: 'add-service-modal',
  styleUrls: [('./default-modal.component.scss')],
  templateUrl: './default-modal.component.html'
})

export class DefaultModal2 implements OnInit {

  modalHeader: string;
  modalContent: string;
  pregunta: Pregunta;
  opciones: Opciones;
  constructor(
    private activeModal: NgbActiveModal,
    private _preguntaService: PreguntaService,
    private _opcionesService: OpcionesService,
    ){
    this.opciones= new Opciones('','','','','','')
     }

  ngOnInit() {

    if((this.pregunta.tipo == 'opción simple')||(this.pregunta.tipo == 'opción múltiple')||(this.pregunta.tipo == 'numérica')){
      this.getopciones();
    }
    else{
      this.modalContent ='';
    }
  }

  closeModal() {
    this.activeModal.close();
  }

  getopciones(){
    this._opcionesService.getOpciones(this.pregunta._id).subscribe(
      response =>{
        if(!response.opc){
            this.modalContent ="No hay opciones que mostrar";
            console.log(response.message);
        }else{
            console.log(this.pregunta._id)
            console.log('response= '+response.opcion);

            console.log('response= '+response);
            console.log(response.opc.opcion1);
            this.opciones=response.opc;
            
            console.log(this.opciones.opcion1);
            console.log('this= '+this.opciones);
        }
      },
      error =>{
        var errorMessage = <any>error;
          if(errorMessage != null){
            //this.errorMessage = body.message;
            console.log(error);
        }

      }
    )
  }
  

}
