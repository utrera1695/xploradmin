import {Component, OnInit} from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DefaultModal } from './default-modal/default-modal.component';

import {Pregunta} from '../../../../models/pregunta';
import {PreguntaService} from '../../../../services/pregunta.services'
import {Cuestionario} from '../../../../models/cuestionario'
import {CuestionarioService} from '../../../../services/cuestionario.services'
import {OpcionesService} from '../../../../services/opciones.services'

@Component({
  selector: 'consultarC',
  templateUrl: './consultarC.html',  
  styleUrls:['./style.scss']
})

export class ConsultarC implements OnInit{
  
  public cuestionarios: Cuestionario[];
  public preguntas: Pregunta[];
  public nomorePage: boolean;
  public page:number;
  public message: string;
  public n: number;
  public limit: number;
  public cantPage: number;
  public restP: number;
  public p: number;
  constructor(
  	private _cuestionarioService: CuestionarioService,
   	private _preguntaService: PreguntaService,
  	private modalService: NgbModal,
  	private _opcionesService: OpcionesService
    ){
    this.cantPage=0;
    this.n=1;
    this.limit=12;
    this.nomorePage = false;
	  this.message='No hay cuestionarios creados';
    this.restP=this.p=82;
  }
	
	lgModalShow(cuestionario) {
    	const activeModal = this.modalService.open(DefaultModal, {size: 'lg'});activeModal.componentInstance.modalHeader = 'Contenido del cuestionario';
      activeModal.componentInstance.modalHeader = 'Contenido del cuestionario: '+cuestionario.nombreC;
      activeModal.componentInstance.cuestionario = cuestionario;
  		activeModal.componentInstance.show = 'show';
      
  	}
  lgModalShowEdit(cuestionario) {
      const activeModal = this.modalService.open(DefaultModal, {size: 'lg'});
        activeModal.componentInstance.modalHeader = 'Editar: '+cuestionario.nombreC;
        activeModal.componentInstance.cuestionario = cuestionario;
        activeModal.componentInstance.show = 'edit';
      
  }
  lgModalDeleteEdit(cuestionario) {
      const activeModal = this.modalService.open(DefaultModal, {size: 'lg'});
        activeModal.componentInstance.modalHeader = 'Eliminar cuestionario: '+cuestionario.nombreC;
        activeModal.componentInstance.cuestionario = cuestionario;
        activeModal.componentInstance.show = 'delete';
      
  }
	ngOnInit(){
		this.listCuestionarios();
	}
	
	public confirm;
	onDeleteConfirm(id){
		this.confirm = id;
	}
	onCancelDelete(){
		this.confirm = null;
	}
  
	listCuestionarios(){
		this._cuestionarioService.listCuestionario(JSON.parse(localStorage.getItem('identity')).empresa).subscribe(
  			response =>{
  				if(!response.cuestion){
  					console.log(response);
       		}else{
  					this.cuestionarios = response.cuestion;
            if(this.cuestionarios.length>0)
              this.message='';

            this.cantPage = response.total/this.limit;
            
          }
	          
	        },
	        error=>{
	          var errorMessage = <any>error;
	          if(errorMessage != null){
	            //this.errorMessage = body.message;
	            console.log(error);
	       		}
	        }
  		);
	};

	clonar(cuestionarioId,cuestionarioName){
    	let cuestionarioC = new Cuestionario('',cuestionarioName+ ' - copia','',JSON.parse(localStorage.getItem('identity')).empresa);
    	this._cuestionarioService.save(cuestionarioC).subscribe(
        	response =>{
          		let cuest = response.cuestionario;
          		
          		if(!cuest._id){
            	//mensaje de error
            		alert('Error al clonar cuestionario')
          		}else{
          			this.listarPreguntas(cuestionarioId,cuest._id);
          			this.cuestionarios.push(cuest);
          		}
        	},
        	error=>{
          		var errorMessage = <any>error;
          		if(errorMessage != null){
            		//this.errorMessage = body.message;
            		console.log(error);
       	  		}
        	}
      	);
  	}
  	listarPreguntas(cuestionarioId,cuestid){
  		this._preguntaService.listPreguntas(cuestionarioId).subscribe(
        	response=>{
        		if(!response.pregunta){
        			//console.log(response);
        		}else{
        			
        			this.preguntas = response.pregunta;
              var id='';
        			for (var i = 0; i<this.preguntas.length;i++){
        				let pregN = this.preguntas[i];
                id=this.preguntas[i]._id
                pregN._id ='';
          			pregN.cuestionario = cuestid;
       					this.clonarP(pregN,id);	
          			}
          		}
          	},
          	error=>{
          		var errorMessage = <any>error;
          		if(errorMessage != null){
            		//this.errorMessage = body.message;
            		console.log(error);
       			}
   			}
   		)     			
  	}
  	clonarP(pregunta,id){
  		this._preguntaService.save(pregunta).subscribe(
			response =>{
				if(!response.pregunta){
					alert("no se pudieron clonar las preguntas")
				}
				else{
          let preg= response.pregunta
          if((pregunta.tipo=='numérica')||(pregunta.tipo=='opción simple')||(pregunta.tipo=='opción múltiple')){
            this._opcionesService.getOpciones(id).subscribe(
              response=>{
                if(response.opc){
                  var opcClone = response.opc;
                  opcClone._id='';
                  opcClone.pregunta=preg._id;
                  console.log(preg._id);
                  this.clonarOpc(opcClone);
                }
              }
            )
          }
				}
			},
			error=>{
				var errorMessage = <any>error;
				if(errorMessage != null){
			//this.errorMessage = body.message;
				console.log(error);
					}
				}
			)
  	}
    clonarOpc(opc){
      this._opcionesService.save(opc).subscribe(
        response=>{
          if(!response.opciones){
            alert('No se ha podido clonar las opciones')
          }else{
            console.log(response.opciones);
          }
          
        }
      )
    }
  next(){
    if(this.n<this.cantPage)
      this.n+=1;
  }
  prev(){
    if(this.n!=1){
      this.n-=1;
    }
  }
}