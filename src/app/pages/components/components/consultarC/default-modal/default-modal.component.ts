import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {Pregunta} from '../../../../../models/pregunta';
import {PreguntaService} from '../../../../../services/pregunta.services'
import {Cuestionario} from '../../../../../models/cuestionario'
import {Opciones} from '../../../../../models/opciones';
import {OpcionesService} from '../../../../../services/opciones.services'
import {CuestionarioService} from '../../../../../services/cuestionario.services'

@Component({
  selector: 'add-service-modal',
  styleUrls: [('./default-modal.component.scss')],
  templateUrl: './default-modal.component.html'
})

export class DefaultModal implements OnInit {

  modalHeader: string;
  modalContent: string;
  cuestionario: Cuestionario;
  show: string;
  public preguntas: Pregunta[];
  public agregar: boolean;
  public pregunta: Pregunta;
  public opciones: Opciones;
  public deletep:boolean;
  public deleteshow:boolean;
  public id: string;
  public restP:number;
  public p: number;
  public edit: boolean;
  public pregunta2: Pregunta;
  public optionUpdate: Opciones;
  public options = [
    {value: 'si y no', display:'Si y No'},
    {value: 'opción simple', display:'Opción Simple'},
    {value: 'opción múltiple', display:'Opción Múltiple'},
    {value: 'respuesta libre', display:'Respuesta Libre'},
    {value: 'numérica', display:'Numérica'}
  ];
  
  constructor(
    private activeModal: NgbActiveModal,
    private _preguntaService: PreguntaService,
    private opcionesService: OpcionesService,
    private _cuestionarioService: CuestionarioService
    
    ){
    this.show='';
    this.agregar=false;
    this.pregunta= new Pregunta('','','','');
    this.opciones = new Opciones('','','','','','')
    this.pregunta2= new Pregunta('','','','');
    this.optionUpdate = new Opciones('','','','','','')
    
    this.deletep=false;
    this.deleteshow=false;
    this.restP=this.p=82;
   }

  ngOnInit() {
    
    this.listPregunta();
  }

  closeModal() {
    this.activeModal.close();
  }

  frasep(){
      this.p=this.restP-this.pregunta.nombre.length;
  }
  newP(){
    if(this.agregar==false){
      this.agregar=true;
    }else{
      this.agregar=false;
    }
  }

  listPregunta(){
    this._preguntaService.listPreguntas(this.cuestionario._id).subscribe(
        response =>{
          this.preguntas=response.pregunta;
          if(!response.pregunta){
           this.modalContent='No hay preguntas'
          }else{
            this.preguntas = response.pregunta;
          }
            
          },
          error=>{
            var errorMessage = <any>error;
            if(errorMessage != null){
              //this.errorMessage = body.message;
              console.log(error);
             }
          }
    );

  }
  update(){
    this._cuestionarioService.update(this.cuestionario).subscribe(
      response=>{
        if(!response.cuestionario){
          alert(response.message);
        }else{

    //let win = (window as any);
    //win.location.reload();
          this.closeModal();
        }
      },  
      error=>{
        var errorMessage = <any>error;
        if(errorMessage != null){
          //this.errorMessage = body.message;
          console.log(error);
        }   
      }
    )
  }
  borrarp(preguntaId){
    this.id=preguntaId;
    if(this.deletep){
      this._preguntaService.delete(preguntaId).subscribe(
        response =>{
          if(!response.pregunta){
            alert('Error al eliminar la pregunra');
          }else{
            this.deletep=false;
            this.id='';
            this.deleteshow=false;
            this.listPregunta();
          }
        },
        error =>{
          var errorMessage = <any>error;
                if(errorMessage != null){
                  //this.errorMessage = body.message;
                  console.log(error);
               }
        }
      )
    }
  }
  
  
  public errorP: string;
  crearPregunta(){
    if(this.cuestionario._id != ''){
      if(this.preguntas.length<5){
        if((this.pregunta.nombre != '')&&(this.pregunta.tipo != '')){
          this.errorP = '';
        this.pregunta.cuestionario = this.cuestionario._id;
        if((this.pregunta.tipo == 'opción simple')||(this.pregunta.tipo == 'opción múltiple')||(this.pregunta.tipo == 'numérica')){
          if(((this.opciones.opcion1!='')&&(this.opciones.opcion2!=''))||((this.opciones.opcion3!='')||(this.opciones.opcion4!=''))){
          if((this.pregunta.tipo == 'numérica')&&(this.opciones.opcion1>this.opciones.opcion2)){}else{   
            this._preguntaService.save(this.pregunta).subscribe(
              response =>{
                let preg = response.pregunta;
                if(!preg._id){
                  alert('Error al crear pregunta');
                }else{
                  this.pregunta = preg;
                  this.preguntas.push(preg);
                  this.pregunta = new Pregunta('','','','');
                    this.opciones.pregunta= preg._id;
                    console.log(this.opciones);
                    this.opcionesService.save(this.opciones).subscribe(
                      response =>{
                        if(!response.opciones){
                          alert('no se pudo crear las opciones');
                        }
                        else{
                          console.log(response);                        
                          this.agregar=false;
                          this.opciones = new Opciones('','','','','','')
                        }
                      },
                      error =>{
                        var errorMessage = <any>error;
                              if(errorMessage != null){
                                //this.errorMessage = body.message;
                                console.log(error);
                               }
                      }
                    )
                }
              },
              error =>{
                var errorMessage = <any>error;
                      if(errorMessage != null){
                        //this.errorMessage = body.message;
                        console.log(error);
                       }
              }
            )
          }
        }else{
          this.errorP= 'Debe rellenar todos los campos antes de guardar'  
        
        }
      }else{
        this._preguntaService.save(this.pregunta).subscribe(
          response =>{
            let preg = response.pregunta;
            if(!preg._id){
              alert('Error al crear pregunta');
            }else{
              this.pregunta = preg;
              this.preguntas.push(preg);              
              this.agregar=false;
              this.pregunta = new Pregunta('','','','');
            }
          },
          error=>{

          }
        )
      }

        }else{
          this.errorP= 'Debe rellenar todos los campos antes de guardar'  
        }
      }else{
        this.errorP= 'Solo se permiten 5 preguntas por cuestionario'
      }
    }else{
      alert('Debe darle nombre a un cuestionario primero');
    }
  }
  delete(){
    this._cuestionarioService.delete(this.cuestionario._id).subscribe(
      response =>{
        if(!response.cuestionario){
          alert('Error al eliminar cuestionario');
        }else{
          let win = (window as any);
          win.location.reload();
        }
      },
      error =>{
        var errorMessage = <any>error;
          if(errorMessage != null){
            //this.errorMessage = body.message;
            console.log(error);
         }
      }
    );
  }
  validateP(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode( key );
    var regex = /[0-9]/;
    if( !regex.test(key) ) {
      theEvent.returnValue = false;
      if(theEvent.preventDefault) theEvent.preventDefault();
    }
  }
  editP(pregunta){
    this.pregunta2 = pregunta;
    this.edit=true;
    if((this.pregunta2.tipo == 'opción simple')||(this.pregunta2.tipo == 'opción múltiple')||(this.pregunta2.tipo == 'numérica')){
      this.opcionesService.getOpciones(pregunta._id).subscribe(
        response=>{
          if(response.opc){
            this.optionUpdate=response.opc;
          }
        }
      )
    }
  }
  updateP(){
    
      if((this.pregunta2.nombre != '')&&(this.pregunta2.tipo != '')){
          this.errorP = '';
        if((this.pregunta2.tipo == 'opción simple')||(this.pregunta2.tipo == 'opción múltiple')||(this.pregunta2.tipo == 'numérica')){
          if(((this.optionUpdate.opcion1!='')&&(this.optionUpdate.opcion2!=''))||((this.optionUpdate.opcion3!='')||(this.optionUpdate.opcion4!=''))){
          if((this.pregunta2.tipo == 'numérica')&&(this.opciones.opcion1>this.opciones.opcion2)){
            this.errorP='El valor minimo no puede ser mayor al maximo';  
          }else{   
            this._preguntaService.update(this.pregunta2).subscribe(
              response =>{
                let preg = response.pregunta;
                if(!preg._id){
                  alert('Error al crear pregunta');
                }else{
                  this.pregunta2 = new Pregunta('','','','');
                  //this.listPregunta();

                  this.opcionesService.update(this.optionUpdate).subscribe(
                    response =>{
                        if(!response.opcion){
                          alert('no se pudo crear las opciones');
                        }
                        else{
                          this.edit=false;
                          this.optionUpdate = new Opciones('','','','','','')
                          this.listPregunta();
                        }
                      },
                      error =>{
                        var errorMessage = <any>error;
                              if(errorMessage != null){
                                //this.errorMessage = body.message;
                                console.log(error);
                               }
                      }
                    )
                }
              },
              error =>{
                var errorMessage = <any>error;
                      if(errorMessage != null){
                        //this.errorMessage = body.message;
                        console.log(error);
                       }
              }
            )
          }
        }else{
          this.errorP= 'Debe rellenar todos los campos antes de guardar'  
        
        }
      }else{
        this._preguntaService.update(this.pregunta2).subscribe(
          response =>{
            let preg = response.pregunta;
            if(!preg._id){
              alert('Error al crear pregunta');
            }else{
              this.pregunta2 = new Pregunta('','','','');
              this.edit=false;                          
              this.listPregunta();
            }
          },
          error=>{

          }
        )
      }

        }else{
          this.errorP= 'Debe rellenar todos los campos antes de guardar'  
        }
      
    
  }
}
