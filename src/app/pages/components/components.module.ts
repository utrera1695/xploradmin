import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';
import { TreeModule } from 'ng2-tree';
import { NgbDropdownModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';


import { routing }       from './components.routing';
import { Components } from './components.component';
import { TreeView } from './components/treeView/treeView.component';
import { DefaultModal } from './components/consultarC/default-modal/default-modal.component';
import { DefaultModal2 } from './components/treeView/default-modal/default-modal.component';
import { ConsultarC } from './components/consultarC/consultarC.component';

import {DragulaModule, DragulaService} from 'ng2-dragula/ng2-dragula'
import {CuestionarioService} from '../../services/cuestionario.services'
import {PreguntaService} from '../../services/pregunta.services'
import {OpcionesService} from '../../services/opciones.services'
import {ReporteService} from '../../services/reporte.services'
import {AuthGuard} from '../../guards/index';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgaModule,
    TreeModule,
    DragulaModule,
    NgbDropdownModule,
    NgbModalModule,
    routing
  ],
  declarations: [
    Components,
    TreeView,
    ConsultarC,
    DefaultModal,
    DefaultModal2,
  ],
  entryComponents: [
    DefaultModal,
    DefaultModal2
  ],
  providers:[
    CuestionarioService,
    OpcionesService,
    PreguntaService,
    ReporteService,
    AuthGuard
  ]
})
export class ComponentsModule {}
