import { Routes, RouterModule }  from '@angular/router';

import { Editors } from './editors.component';
import { Ckeditor } from './components/ckeditor/ckeditor.component';
import { SmartTables } from './components/smartTables/smartTables.component';
import { Empresas } from './components/empresa/empresa.component'
import {AuthGuard} from '../../guards/index';
import {Sesions} from './components/sesions/sesions.component'
// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: Editors,
    children: [
      { path: 'ckeditor', component: Ckeditor },
   	  { path: 'ConsultarAdmin', component: SmartTables},
   	  { path: 'Empresas', component: Empresas},
      { path: 'Sesions', component: Sesions}
    ],
    canActivate: [AuthGuard]
  }
];

export const routing = RouterModule.forChild(routes);
