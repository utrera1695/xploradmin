import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { CKEditorModule } from 'ng2-ckeditor';
import { NgaModule } from '../../theme/nga.module';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { DefaultModalAdmin } from './components/smartTables/default-modal/default-modal.component';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import {AuthGuard} from '../../guards/index';

import { routing }       from './editors.routing';
import { Editors } from './editors.component';
import { Ckeditor } from './components/ckeditor/ckeditor.component';
import { SmartTables } from './components/smartTables/smartTables.component';
import { Empresas } from './components/empresa/empresa.component'
import { Sesions } from './components/sesions/sesions.component'
import { AdminService } from '../../services/admin.services'
import { EmpresaService } from '../../services/empresa.services'
import {DataTableFilterPipe} from './components/smartTables/dataTable.pipe'
import {DataTableEmpreFilterPipe} from './components/empresa/dataTable.pipe'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgaModule,
    ReactiveFormsModule,
    routing,
    NgbModalModule,
  ],
  declarations: [
    Editors,
    SmartTables,
    Ckeditor,
    Empresas,
    Sesions,
    DefaultModalAdmin,
    DataTableFilterPipe,
    DataTableEmpreFilterPipe
  ],
  providers: [
    AdminService,
    EmpresaService,
    AuthGuard
  ],
  entryComponents: [
    DefaultModalAdmin
  ],
})

export class EditorsModule {
}
