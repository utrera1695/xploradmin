import { Component } from '@angular/core';
import { Empresa } from '../../../../models/empresa'
import {EmpresaService} from '../../../../services/empresa.services'


@Component({
  selector: 'empresas',
    templateUrl: './empresa.html',
    styleUrls: ['./empresa.scss']
})
export class Empresas {
	filter: Empresa = new Empresa('','','',0,'');
	public empresas: Empresa[];
	public empresa: Empresa;
	public cantPage: number;
	public n: number;
	public limit: number;
	public edit: boolean;
	public elim: boolean;
	constructor(
		private _empresaService:EmpresaService 
	){
		this.empresas= new Array<Empresa>()
		this.empresa= new Empresa('','','',2,'');
		this.listEmpresas();
		this.cantPage=0;
       	this.n=1;  
       	this.limit=10;
       	this.edit=false;

	}

	public emprecant=0;
	public listEmpresas(){
	    this._empresaService.listEmpresa().subscribe(
		  response=>{
	        if(response.empresa){
	          this.empresas=response.empresa;
	          this.emprecant=response.total;	          
              this.cantPage = response.total/this.limit;
	        }
	      },
	      error=>{
	        var errorMessage = <any>error;
	          if(errorMessage != null){
	            //this.errorMessage = body.message;
	            console.log(error);
	          }
	      }
	    )
	}
	next(){
    	if(this.n<this.cantPage)
      		this.n+=1;
	}
	prev(){
	    if(this.n!=1){
	      this.n-=1;
	    }
	}
	public message='';
	public saveEmpresa(){
		this.editnombre=false;
		if(this.empresa.nombre!=''){
			this._empresaService.getEmpresaname(this.empresa.nombre).subscribe(
				response=>{
					if(!response.empresa){
						this.setdata()
						if(this.empresa.tipoK!=''){
							if(this.empresa.maxboton>=2){
								if(this.empresa.mapa!=''){
									this._empresaService.save(this.empresa).subscribe(
										response=>{
											if(!response.empresa){
												this.message='No se pudo guardar la empresa'
											}else{
												this.empresa= new Empresa('','','',2,'');
												this.newEmp=false;
												this.checkbox2=false;
												this.checkbox1=false;
												this.message='';
												this.errormap=this.errorcant=this.errornombre=false;
												this.listEmpresas();
											}
										},
										error=>{
											var errorMessage = <any>error;
									          if(errorMessage != null){
									            //this.errorMessage = body.message;
									            console.log(error);
									        }
										}
									)
								}else{
									this.errormap=true;
									this.message='Debe ingresar una direccion de mapa'
								}
							}else{
								this.errorcant=true;
								this.message='Debe ingresar una cantidad de botones mayor o igual a dos'
							}
						}else{
							this.message='Debe seleccionar un tipo de kiosko'
						}
					}else{
						this.errornombre=true;
						this.message='Este nombre de empresa ya existe'
					}
				}
			)
		}else{
			this.errornombre=true;
			this.message='Debe ingresar el nombre de la empresa antes de guardar'
		}
	}
	public editEmpresa(){
		if(this.empresa.nombre!=''){
			if(this.empresa.tipoK!=''){
				if(this.empresa.maxboton>=2){
					if(this.empresa.mapa!=''){
						this._empresaService.getEmpresaname(this.empresa.nombre).subscribe(
							response=>{
								if(!response.empresa){
									this._empresaService.update(this.empresa).subscribe(
										response=>{
											if(!response.empresa){
												this.message='No se pudo guardar la empresa'
											}else{
												this.empresa= new Empresa('','','',2,'');
												this.edit=false;
												this.checkbox2=false;
												this.checkbox1=false;
												this.errormap=this.errorcant=this.errornombre=false;
												this.listEmpresas();
											}
										},
										error=>{
											var errorMessage = <any>error;
									          if(errorMessage != null){
									            //this.errorMessage = body.message;
									            console.log(error);
									        }
										}
									)
								}else{
									if(((!this.editnombre)&&(response.empresa.nombre==this.empresa.nombre))||((this.editnombre)&&(response.empresa.nombre!=this.empresa.nombre))){
										this._empresaService.update(this.empresa).subscribe(
											response=>{
												if(!response.empresa){
													this.message='No se pudo guardar la empresa'
												}else{
													this.empresa= new Empresa('','','',2,'');
													this.edit=false;
													this.checkbox2=false;
													this.checkbox1=false;
													this.errormap=this.errorcant=this.errornombre=false;
													this.listEmpresas();
												}
											},
											error=>{
												var errorMessage = <any>error;
										          if(errorMessage != null){
										            //this.errorMessage = body.message;
										            console.log(error);
										        }
											}
										)
									}else{
										this.errornombre=true;
										this.message='Este nombre de empresa ya existe'
									}
								}
							}
						)
					}else{
						this.errormap=true;
						this.message='Debe ingresar una direccion de mapa'
					}
				}else{
					this.errorcant=true;
					this.message='Debe ingresar una cantidad de botones mayor o igual a dos'
				}
			}else{
				this.message='Debe seleccionar un tipo de kiosko'
			}
		}else{
			this.errornombre=true;
			this.message='Debe ingresar el nombre de la empresa antes de guardar'
		}
	}
	public confirmelimemp(empresa){
		this._empresaService.delete(empresa).subscribe(
			response=>{
				if(response.empresa){
					this.listEmpresas();
					this.elim=false;					
					this.empresa= new Empresa('','','',2,'');
				}
			},
			error=>{

			}
		)
	}
	public elimemp(empresa){
		if((this.newEmp==false)&&(this.edit==false)){
			this.elim=true;
			this.empresa=empresa;
		}
	}
	public newEmp=false;
	public newEmpresa(){
		if(this.newEmp==false){
			this.newEmp=true;
		}else{
			this.newEmp=false;
			this.empresa= new Empresa('','','',2,'');
		}						
	}
	public cancel(){
		this.newEmp=false;
		this.errormap=this.errorcant=this.errornombre=false;
		this.empresa= new Empresa('','','',2,'');
		this.edit=false;
		this.elim=false;
		this.checkbox2=false;
		this.checkbox1=false;
		this.listEmpresas();
	}
	checkbox1=false;
	public check1(){
		if(this.checkbox1==false){
			this.checkbox1=true
		}else
			this.checkbox1=false

	}
	checkbox2=false
	public check2(){
		if(this.checkbox2==false){
			this.checkbox2=true
		}else
			this.checkbox2=false
	}

	public setdata(){
		if(this.checkbox1){
			if(this.checkbox2){
				this.empresa.tipoK='Multiple'
			}else{
				this.empresa.tipoK='Formulario'
			}
		}else{
			if(this.checkbox2){
				this.empresa.tipoK='Directorio'
			}else{
				this.empresa.tipoK=''
			}
		}

	}

	public editemp(empresa){
		if(this.newEmp==false){
			this.edit=true;		
			this.empresa=empresa;
		}
	}
	errornombre=false;
	errorcant=false;
	errormap =false
	nombreError(){
	    var style={
	      'border-color': this.errornombre ? '#FFC107':'#3c444a' 
	    }
	    return style;
	}
	cantError(){
	    var style={
	      'border-color': this.errorcant ? '#FFC107':'#3c444a'
	    }
	    return style;
	}
	mapError(){
	    var style={
	      'border-color': this.errormap ? '#FFC107':'#3c444a'
	    }
	    return style;
	}
	validateNum(evt) {
	    var theEvent = evt || window.event;
	    var key = theEvent.keyCode || theEvent.which;
	    key = String.fromCharCode( key );
	    var regex = /[0-7]/;
	    if( !regex.test(key) ) {
	      theEvent.returnValue = false;
	      if(theEvent.preventDefault) theEvent.preventDefault();
	    }
	  }
	public editnombre= false;
  	validateN(evt) {
  		this.editnombre=true;
	    var theEvent = evt || window.event;
	    var key = theEvent.keyCode || theEvent.which;
	    key = String.fromCharCode( key );
	    var regex = /[a-záéíóúñ]|[A-ZÁÉÍÓÚÑ]|[0-9]|\ /;
	    if( !regex.test(key) ) {
	      theEvent.returnValue = false;
	      if(theEvent.preventDefault) theEvent.preventDefault();
	    }
	  }
}
