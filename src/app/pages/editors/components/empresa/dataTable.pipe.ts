import { Pipe, PipeTransform } from '@angular/core';

import { Empresa } from '../../../../models/empresa';

@Pipe({
    name: 'empresafilter',
    pure: false
})
export class DataTableEmpreFilterPipe implements PipeTransform {
  transform(items: Empresa[], filter: Empresa): Empresa[] {
    if (!items || !filter) {
      return items;
    }
    // filter items array, items which match and return true will be kept, false will be filtered out
    return items.filter((item: Empresa) => this.applyFilter(item, filter));
  }
  
  /**
   * Perform the filtering.
   * 
   * @param {Admin} book The book to compare to the filter.
   * @param {Admin} filter The filter to apply.
   * @return {boolean} True if book satisfies filters, false if not.
   */
  applyFilter(empresa: Empresa, filter: Empresa): boolean {
    for (let field in filter) {
      if (filter[field]) {
        if (typeof filter[field] === 'string') {
          if (empresa[field].toLowerCase().indexOf(filter[field].toLowerCase()) === -1) {
            return false;
          }
        } else if (typeof filter[field] === 'number') {
          if (empresa[field] !== filter[field]) {
            return false;
          }
        }
      }
    }
    return true;
  }
}