import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgUploaderOptions } from 'ngx-uploader';
import { Admin } from '../../../../../models/admin'
import { AdminService } from '../../../../../services/admin.services'
import {Empresa} from '../../../../../models/empresa'
import {EmpresaService} from '../../../../../services/empresa.services'

@Component({
  selector: 'add-service-modal',
  styleUrls: [('./default-modal.component.scss')],
  templateUrl: './default-modal.component.html'
})

export class DefaultModalAdmin implements OnInit {

  modalHeader: 'Crear nueva pregunta';
  modalContent: 'hola vale';
  show: string;
  public admin: Admin;
  public message:string; 
  public empresas: Empresa[];
  public emprecant: number;
  public fileUploaderOptions:NgUploaderOptions = {
    // url: 'http://website.com/upload'
    url: '',
  };
  constructor(
    private activeModal: NgbActiveModal,
    private _adminService: AdminService,
    private _empresaService: EmpresaService
  ){
    this.admin = new Admin('','','','','','','','');
    this.myacount();
    this.message='';
    this.empresas = new Array<Empresa>()
    this.emprecant=0;
    this.listEmpresas()
  }

  ngOnInit() {
   //this.listPregunta();
  }

  closeModal() {
    if(this.show=='edit'){
      let win = (window as any);
      win.location.reload();
    }
    this.activeModal.close();
  }
  public listEmpresas(){
    this._empresaService.listEmpresa().subscribe(
      response=>{
        if(response.empresa){
          this.empresas=response.empresa;
          this.emprecant=response.total;
        }
      },
      error=>{
        var errorMessage = <any>error;
          if(errorMessage != null){
            //this.errorMessage = body.message;
            console.log(error);
          }
      }
    )
  }
  myacount(){
    var mya = JSON.parse(localStorage.getItem('identity'))
    if(mya._id == this.admin._id){
      this.show='deletemyacount';
    }
  }
  delete(){
      if(this.show=='deletemyacount'){
        this._adminService.delete(this.admin._id).subscribe(
          response=>{
            if(!response.admin){
              alert(response);
            }else{
              localStorage.removeItem('identity');
              localStorage.removeItem('token');
              localStorage.clear();
            }
          },
          error=>{
            var errorMessage = <any>error;
                if(errorMessage != null){
                  //this.errorMessage = body.message;
                  console.log(error);
                }  
          }
        )
      }else{
        this._adminService.delete(this.admin._id).subscribe(
          response=>{
            if(!response.admin){
              alert(response);
            }else{
              let win = (window as any);
              win.location.reload();
            }
          },
          error=>{
            var errorMessage = <any>error;
                if(errorMessage != null){
                  //this.errorMessage = body.message;
                  console.log(error);
                }  
          }
        )
      }
  }
  public editEmail=false;
  public update(){
    if(this.notNull()){
      this._adminService.getAdmin(this.admin.username).subscribe(
      response=>{
         if(!response.admin){
           if(this.admin.email.search('@')>4){
                if(this.admin.pass.length>=4){
                  if(this.validateCharacter()){
                      if(this.editEmail==false){
                          this.admin.password=this.admin.pass;
                          this._adminService.update(this.admin).subscribe(
                              response =>{
                                let admin = response.adminU;
                                
                                if(!admin._id){
                                  //mensaje de error
                                  this.message =response.message;
                                }else{
                                  this.admin = admin;
                                  //mensaje de correcto
                                  this.message=this.admin.name +' Se ha registrado sin problemas';
                                  this.admin = new Admin('','','','','','','','');
                                  this.activeModal.close();
                                }
                              },
                              error=>{
                                var errorMessage = <any>error;
                                if(errorMessage != null){
                                  //this.errorMessage = body.message;
                                  console.log(error);
                                }
                              }
                            );
                      }else{
                        this._adminService.getAdminEmail(this.admin.email).subscribe(
                          response=>{
                            if(!response.admin){
                              this.admin.password=this.admin.pass;
                              this._adminService.update(this.admin).subscribe(
                                  response =>{
                                    let admin = response.adminU;
                                    
                                    if(!admin._id){
                                      //mensaje de error
                                      this.message =response.message;
                                    }else{
                                      this.admin = admin;
                                      //mensaje de correcto
                                      this.message=this.admin.name +' Se ha registrado sin problemas';
                                      this.admin = new Admin('','','','','','','','');
                                      this.activeModal.close();
                                    }
                                  },
                                  error=>{
                                    var errorMessage = <any>error;
                                    if(errorMessage != null){
                                      //this.errorMessage = body.message;
                                      console.log(error);
                                    }
                                  }
                                );
                            }else{
                              this.message='Esta cuenta de correo electrónico ya le pertenece a otro usuario'
                            }
                          }
                        )
                      }
                  }
              }else{
                this.message='Para Mayor Seguridad su contraseña debe de ser mayor a 4 caracteres alfanumericos'; 
              }
            }else{
              this.message='Por favor introduzca una direcíón de correo electrónico valido';
            }
        }else{
          if(((!this.editUsername)&&(this.admin.username == response.admin.username))||((this.editUsername)&&(this.admin.username != response.admin.username))){
            if(this.admin.email.search('@')>4){
                if(this.admin.pass.length>=4){
                  if(this.validateCharacter()){
                      if(this.editEmail==false){
                          this.admin.password=this.admin.pass;
                          this._adminService.update(this.admin).subscribe(
                              response =>{
                                let admin = response.adminU;
                                
                                if(!admin._id){
                                  //mensaje de error
                                  this.message =response.message;
                                }else{
                                  this.admin = admin;
                                  //mensaje de correcto
                                  this.message=this.admin.name +' Se ha registrado sin problemas';
                                  this.admin = new Admin('','','','','','','','');
                                  this.activeModal.close();
                                }
                              },
                              error=>{
                                var errorMessage = <any>error;
                                if(errorMessage != null){
                                  //this.errorMessage = body.message;
                                  console.log(error);
                                }
                              }
                            );
                      }else{
                        this._adminService.getAdminEmail(this.admin.email).subscribe(
                          response=>{
                            if(!response.admin){
                              this.admin.password=this.admin.pass;
                              this._adminService.update(this.admin).subscribe(
                                  response =>{
                                    let admin = response.adminU;
                                    
                                    if(!admin._id){
                                      //mensaje de error
                                      this.message =response.message;
                                    }else{
                                      this.admin = admin;
                                      //mensaje de correcto
                                      this.message=this.admin.name +' Se ha registrado sin problemas';
                                      this.admin = new Admin('','','','','','','','');
                                      this.activeModal.close();
                                    }
                                  },
                                  error=>{
                                    var errorMessage = <any>error;
                                    if(errorMessage != null){
                                      //this.errorMessage = body.message;
                                      console.log(error);
                                    }
                                  }
                                );
                            }else{
                              this.message='Esta cuenta de correo electrónico ya le pertenece a otro usuario'
                            }
                          }
                        )
                      }
                  }
              }else{
                this.message='Para Mayor Seguridad su contraseña debe de ser mayor a 4 caracteres alfanumericos'; 
              }
            }else{

              this.message='Por favor introduzca una direcíón de correo electrónico valido';
            }
          }else{
            this.message='Ya existe ese nombre de usuario';
          }

        }
        
      })
    }else{
      this.message= 'Debe rellenar algunos campos obligatorios antes de guardar';
    }
  }
  notNull(){
    if((this.admin.name!='')&&(this.admin.username!='')&&(this.admin.pass!='')){
      return true;
    }
    return false;
  }
  validateCharacter(){
    if(this.admin.name.length>=4){
      if(this.admin.username.length>=4){
        return true;
      }else{
        this.message='Su nombre de usuario debe de ser mayor a cuatro caracteres';
        return false;;
      }
    }else{
      this.message='Su nombre y apellido debe de ser mayor a cuatro caracteres';
      return false;
    }
  }
  public editUsername=false;
  validateP(evt) {
    this.editUsername=true;
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode( key );
    var regex = /[0-9]|[a-z]|[A-Z]/;
    if( !regex.test(key) ) {
      theEvent.returnValue = false;
      if(theEvent.preventDefault) theEvent.preventDefault();
    }
  }
  validateN(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode( key );
    var regex = /[a-záéíóúñ]|[A-ZÁÉÍÓÚÑ]|\ /;
    if( !regex.test(key) ) {
      theEvent.returnValue = false;
      if(theEvent.preventDefault) theEvent.preventDefault();
    }
  }

}
