import { Component } from '@angular/core';
import { DefaultModalAdmin } from './default-modal/default-modal.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { Admin } from '../../../../models/admin' 
import { AdminService } from '../../../../services/admin.services'


@Component({
  selector: 'smart-tables',
    templateUrl: './smartTables.html',
  styleUrls: ['./smartTables.scss']
})
export class SmartTables {
	public admins: Admin[];
  filter: Admin = new Admin('','','','','','','','');
  public n: number;
  public cantPage: number;
  public limit: number;
  	constructor(
  		private _adminService: AdminService,
  		private modalService: NgbModal
    ){
      this.admins = new Array<Admin>();
 		  this.listAdmins();  
       this.cantPage=0;
       this.n=1;  
       this.limit=10;
  	}

  	lgModalShow(admin) {
      const activeModal = this.modalService.open(DefaultModalAdmin, {size: 'lg'}); 
      activeModal.componentInstance.modalHeader = 'Editar Administrador';
      activeModal.componentInstance.admin = admin;
      activeModal.componentInstance.show = 'edit';
   	}
    lgModalDelete(admin) {
      const activeModal = this.modalService.open(DefaultModalAdmin, {size: 'lg'}); 
      activeModal.componentInstance.modalHeader = 'Eliminar Administrador';
      activeModal.componentInstance.admin = admin;
      activeModal.componentInstance.show = 'delete';
    }
  	listAdmins(){

  		this._adminService.listAdmins().subscribe(
  			response=>{
  				if(!response.admins){
  					alert(response);
  				}else{
  					this.admins= response.admins;
            console.log(this.admins)
            this.cantPage = response.total/this.limit;
  					
  				}
  			},
  			error=>{
  				var errorMessage = <any>error;
          		if(errorMessage != null){
            		//this.errorMessage = body.message;
            		console.log(error);
          		}
  			}
  		)
  	}
  next(){
    if(this.n<this.cantPage)
      this.n+=1;
  }
  prev(){
    if(this.n!=1){
      this.n-=1;
    }
  }
  	
}
