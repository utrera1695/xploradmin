import { Pipe, PipeTransform } from '@angular/core';

import { Admin } from '../../../../models/admin';

@Pipe({
    name: 'adminfilter',
    pure: false
})
export class DataTableFilterPipe implements PipeTransform {
  transform(items: Admin[], filter: Admin): Admin[] {
    if (!items || !filter) {
      return items;
    }
    // filter items array, items which match and return true will be kept, false will be filtered out
    return items.filter((item: Admin) => this.applyFilter(item, filter));
  }
  
  /**
   * Perform the filtering.
   * 
   * @param {Admin} book The book to compare to the filter.
   * @param {Admin} filter The filter to apply.
   * @return {boolean} True if book satisfies filters, false if not.
   */
  applyFilter(admin: Admin, filter: Admin): boolean {
    for (let field in filter) {
      if (filter[field]) {
        if (typeof filter[field] === 'string') {
          if (admin[field].toLowerCase().indexOf(filter[field].toLowerCase()) === -1) {
            return false;
          }
        } else if (typeof filter[field] === 'number') {
          if (admin[field] !== filter[field]) {
            return false;
          }
        }
      }
    }
    return true;
  }
}