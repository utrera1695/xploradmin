import { Component } from '@angular/core';

import { Admin } from '../../../../models/admin' 
import { AdminService } from '../../../../services/admin.services'
import { Empresa } from '../../../../models/empresa'
import {EmpresaService} from '../../../../services/empresa.services'


@Component({
  selector: 'sesions',
    templateUrl: './sesions.html',
  styleUrls: ['./sesions.scss']
})
export class Sesions {
	public empresas: Empresa[];
	public emprecant=0;
  	constructor(
  		private _empresaService:EmpresaService
  	){
  		this.listEmpresas();
  	}

  	public listEmpresas(){
	    this._empresaService.listEmpresa().subscribe(
		  response=>{
	        if(response.empresa){
	          this.empresas=response.empresa;
	          this.emprecant=response.total;	          
	        }
	      },
	      error=>{
	        var errorMessage = <any>error;
	          if(errorMessage != null){
	            //this.errorMessage = body.message;
	            console.log(error);
	          }
	      }
	    )
	}
}
 