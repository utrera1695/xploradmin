import { Component } from '@angular/core';
import { Admin } from '../../../../models/admin'
import {AdminService} from '../../../../services/admin.services'
import {Empresa} from '../../../../models/empresa'
import {EmpresaService} from '../../../../services/empresa.services'
import {FormGroup, AbstractControl, FormBuilder, Validators} from '@angular/forms';

import './ckeditor.loader';
import 'ckeditor';

@Component({
  selector: 'ckeditor-component',
  templateUrl: './ckeditor.html',
  styleUrls: ['./ckeditor.scss']
})

export class Ckeditor {
  public AdminRegister: Admin;
  public message: String;
  public empresas: Empresa[]
  public password2: string;
  public password: string;
  public emprecant: number;
  
  constructor(
    private _adminService:AdminService,
    private _empresaService: EmpresaService

  ){
    this.AdminRegister = new Admin('','','','','','','','');
    this.empresas = new Array<Empresa>()
    this.message='';
    this.password2='';
    this.password='';
    this.emprecant=0;
    this.listEmpresas()
  }
  
  public listEmpresas(){
    this._empresaService.listEmpresa().subscribe(
      response=>{
        if(response.empresa){
          this.empresas=response.empresa;
          this.emprecant=response.total;
        }
      },
      error=>{
        var errorMessage = <any>error;
          if(errorMessage != null){
            //this.errorMessage = body.message;
            console.log(error);
          }
      }
    )
  }

  public onSubmitRegister(){
    this.AdminRegister.pass=this.password;
    this.erroremail=false;
    if(this.notNull()){
      this._adminService.getAdmin(this.AdminRegister.username).subscribe(
      response=>{
        if(!response.admin){
          if(this.AdminRegister.email.search('@')>2){
              if(this.AdminRegister.pass.length>=4){
                if(this.validateCharacter()){
                  if(this.password==this.password2){
                    if(this.AdminRegister.empresa!=''){
                      this._adminService.getAdminEmail(this.AdminRegister.email).subscribe(
                        response=>{
                          if(!response.admin){
                            this.AdminRegister.password=this.AdminRegister.pass;
                            this._adminService.register(this.AdminRegister).subscribe(
                              response =>{
                                let admin = response.admin;
                                
                                if(!admin._id){
                                  //mensaje de error
                                  this.message =response.message;
                                }else{
                                  this.AdminRegister = admin;
                                  //mensaje de correcto
                                  this.message=this.AdminRegister.name +' Se ha registrado sin problemas';
                                  this.AdminRegister = new Admin('','','','','','','','');
                                  this.password=this.password2='';
                                  this.erorrpass=this.erroremail=this.errornombre=this.errorusername=false;
                                }
                              },
                              error=>{
                                var errorMessage = <any>error;
                                if(errorMessage != null){
                                  //this.errorMessage = body.message;
                                  console.log(error);
                                }
                              }
                            );
                          }else{
                            this.erroremail=true;
                            this.message = 'Este correo electrónico ya le pertenece a otro usuario, por favor utilice uno diferente';
                          }
                      })
                    }else{
                      this.erorrempre=true;
                      this.message='Debe seleccionar una empresa para poder guardar';
                    }
                  }else{
                   this.erorrpass=true;
                   this.message='Las contraseñas no coinciden'; 
                  }
                }
            }else{
              this.erorrpass=true;
              this.message='Para Mayor Seguridad su contraseña debe de ser mayor a 4 caracteres alfanumericos'; 
            }
          }else{
            this.erroremail=true;
            this.message='Por favor introduzca una direción de correo electrónico válido';
          }
        }else{
          this.errorusername=true;
          this.message='Ya existe ese nombre de usuario';
        }
      })
    }else{
      this.message= 'Debe rellenar algunos campos obligatorios antes de guardar';
    }
  }
  notNull(){
    if(this.AdminRegister.name!=''){
      this.errornombre=false;
     if(this.AdminRegister.username!=''){
       this.errorusername=false;
       if(this.AdminRegister.pass!=''){
        this.erorrpass=false;
          if(this.AdminRegister.empresa!=''){
            this.erorrempre=false;
            return true;  
          }else{
            this.erorrempre=true;
            return false;
          }        
        }else{
          this.erorrpass=true;
          return false;
        }
      }else{
        this.errorusername=true;
        return false;}
    }else{
      this.errornombre=true;
      return false;}

    
  }
  validateCharacter(){
    if(this.AdminRegister.name.length>=4){
      this.errornombre=false;
      if(this.AdminRegister.username.length>=4){
        this.errornombre=false;
        return true;
      }else{
        this.message='El nombre de usuario debe de ser mayor a cuatro caracteres';
        this.errorusername=true;
        return false;;
      }
    }else{
      this.message='El nombre y apellido debe de ser mayor a cuatro caracteres';
      this.errornombre=true;
      return false;
    }
  }
  public errornombre=false;
  public errorusername=false;
  public erroremail=false;
  public erorrpass=false;
  public erorrempre=false;
  
  empreError(){
    var style={
      'border-color': this.erorrempre ? '#FFC107':'#3c444a'
    }
    return style;
  }
  passError(){
    var style={
      'border-color': this.erorrpass ? '#FFC107':'#3c444a'
    }
    return style;
  }
  nameError(){
    var style={
      'border-color': this.errornombre ? '#FFC107':'#3c444a'
    }
    return style;
  }
  usernameError(){
    var style={
      'border-color': this.errorusername ? '#FFC107':'#3c444a'
    }
    return style;
  }
  emailError(){
    var style={
      'border-color': this.erroremail ? '#FFC107':'#3c444a'
    }
    return style;
  }
  
  validateP(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode( key );
    var regex = /[0-9]|[a-z]|[A-Z]/;
    if( !regex.test(key) ) {
      theEvent.returnValue = false;
      if(theEvent.preventDefault) theEvent.preventDefault();
    }
  }
  validateNum(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode( key );
    var regex = /[0-9]/;
    if( !regex.test(key) ) {
      theEvent.returnValue = false;
      if(theEvent.preventDefault) theEvent.preventDefault();
    }
  }
  validateN(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode( key );
    var regex = /[a-záéíóúñ]|[A-ZÁÉÍÓÚÑ]|\ /;
    if( !regex.test(key) ) {
      theEvent.returnValue = false;
      if(theEvent.preventDefault) theEvent.preventDefault();
    }
  }
}
