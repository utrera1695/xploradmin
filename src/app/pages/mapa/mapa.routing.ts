import { Routes, RouterModule }  from '@angular/router';

import { Mapa } from './mapa.component';
import { ModuleWithProviders } from '@angular/core';
import {AuthGuard} from '../../guards/index';

// noinspection TypeScriptValidateTypes
export const routes: Routes = [
  {
    path: '',
    component: Mapa
  }
];

export const routing = RouterModule.forChild(routes);