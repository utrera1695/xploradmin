import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';
import { HttpModule,JsonpModule} from "@angular/http";
import { routing }       from './mapa.routing';
import { Mapa } from './mapa.component';
import { Wayfinder3d } from '../../services/3dwayfinder.services'
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgaModule,
    routing,
    HttpModule,
    JsonpModule
  ],
  declarations: [ Mapa ],
  providers: [ Wayfinder3d ]
})
export class MapaModule {
}
