export const PAGES_MENU = [
  {
    path: 'pages',
    children: [
      {
        path: 'dashboard',
        data: {
          menu: {
            title: 'Dashboard',
            icon: 'ion-android-home',
            selected: false,
            expanded: false,
            order: 0
          }
        }
      },
      {
        path: 'tables',
        data: {
          menu: {
            title: 'general.menu.tables',
            icon: 'ion-happy-outline',
            selected: false,
            expanded: false,
            order: 500,
          }
        }
      },
      {
        path: 'editors',
        data: {
          menu: {
            title: 'general.menu.editors',
            icon: 'ion-person',
            selected: false,
            expanded: false,
            order: 100,
          }
        },
        children: [
          {
            path: 'ckeditor',
            data: {
              menu: {
                title: 'general.menu.ck_editor',
              }
            }
          },
          {
            path: 'ConsultarAdmin',
            data: {
              menu: {
                title: 'general.menu.ConsultarAdmin',
              }
            }
          },
          {
            path: 'Empresas',
            data: {
              menu: {
                title: 'Empresa',
              }
            }
          },
          {
            path: 'Sesions',
            data: {
              menu: {
                title: 'Actividades',
              }
            }
          }
        ]
      },
      {
        path: 'components',
        data: {
          menu: {
            title: 'general.menu.components',
            icon: 'ion-clipboard',
            selected: false,
            expanded: false,
            order: 250,
          }
        },
        children: [
          {
            path: 'treeview',
            data: {
              menu: {
                title: 'general.menu.tree_view',
              }
            }
          },
          {
            path: 'consultarC',
            data: {
              menu: {
                title: 'general.menu.consultarC',
              }
            }
          }
        ]
      },
      {
        path: 'ui',
        data: {
          menu: {
            title: 'general.menu.ui_features',
            icon: 'ion-monitor',
            selected: false,
            expanded: false,
            order: 300,
          }
        }
      },
      {
        path: 'forms',
        data: {
          menu: {
            title: 'general.menu.form_elements',
            icon: 'ion-compose',
            selected: false,
            expanded: false,
            order: 400,
          }
        }
      },
      {
        path: 'charts',
        data: {
          menu: {
            title: 'general.menu.charts',
            icon: 'ion-paintbucket',
            selected: false,
            expanded: false,
            order: 700,
          }
        },
        children: [
          {
            path: 'chartist-js',
            data: {
              menu: {
                title: 'Tema',
              }
            }
          },
          {
            path: 'slideshow',
            data: {
              menu: {
                title: 'SlideShow',
              }
            }
          }
        ]  
      },
      {
        path: 'mapa',
        data: {
          menu: {
            title: 'Mapa',
            icon: 'ion-map',
            selected: false,
            expanded: false,
            order: 800,
          }
        }
      }    
    ]
  }
];

export const PAGES_MENU_A_MULTIPLE = [
  {
    path: 'pages',
    children: [
      {
        path: 'dashboard',
        data: {
          menu: {
            title: 'Dashboard',
            icon: 'ion-android-home',
            selected: false,
            expanded: false,
            order: 0
          }
        }
      },
      {
        path: 'tables',
        data: {
          menu: {
            title: 'general.menu.tables',
            icon: 'ion-happy-outline',
            selected: false,
            expanded: false,
            order: 500,
          }
        }
      },
      {
        path: 'components',
        data: {
          menu: {
            title: 'general.menu.components',
            icon: 'ion-clipboard',
            selected: false,
            expanded: false,
            order: 250,
          }
        },
        children: [
          {
            path: 'treeview',
            data: {
              menu: {
                title: 'general.menu.tree_view',
              }
            }
          },
          {
            path: 'consultarC',
            data: {
              menu: {
                title: 'general.menu.consultarC',
              }
            }
          }
        ]
      },
      {
        path: 'ui',
        data: {
          menu: {
            title: 'general.menu.ui_features',
            icon: 'ion-monitor',
            selected: false,
            expanded: false,
            order: 300,
          }
        }
      },
      {
        path: 'forms',
        data: {
          menu: {
            title: 'general.menu.form_elements',
            icon: 'ion-compose',
            selected: false,
            expanded: false,
            order: 400,
          }
        }
      },
      {
        path: 'charts',
        data: {
          menu: {
            title: 'general.menu.charts',
            icon: 'ion-paintbucket',
            selected: false,
            expanded: false,
            order: 200,
          }
        },
        children: [
          {
            path: 'chartist-js',
            data: {
              menu: {
                title: 'Tema',
              }
            }
          },
          {
            path: 'slideshow',
            data: {
              menu: {
                title: 'SlideShow',
              }
            }
          }
        ]   
      },
      {
        path: 'mapa',
        data: {
          menu: {
            title: 'Mapa',
            icon: 'ion-map',
            selected: false,
            expanded: false,
            order: 700,
          }
        }
      }

    ]
  }
];

export const PAGES_MENU_A_DIRECTORIO = [
  {
    path: 'pages',
    children: [
      {
        path: 'dashboard',
        data: {
          menu: {
            title: 'Dashboard',
            icon: 'ion-android-home',
            selected: false,
            expanded: false,
            order: 0
          }
        }
      },
      {
        path: 'tables',
        data: {
          menu: {
            title: 'general.menu.tables',
            icon: 'ion-happy-outline',
            selected: false,
            expanded: false,
            order: 500,
          }
        }
      },
      {
        path: 'ui',
        data: {
          menu: {
            title: 'general.menu.ui_features',
            icon: 'ion-monitor',
            selected: false,
            expanded: false,
            order: 300,
          }
        }
      },
      {
        path: 'charts',
        data: {
          menu: {
            title: 'general.menu.charts',
            icon: 'ion-paintbucket',
            selected: false,
            expanded: false,
            order: 200,
          }
        },
        children: [
          {
            path: 'slideshow',
            data: {
              menu: {
                title: 'SlideShow',
              }
            }
          }
        ]   
      },
      {
        path: 'mapa',
        data: {
          menu: {
            title: 'Mapa',
            icon: 'ion-map',
            selected: false,
            expanded: false,
            order: 700,
          }
        }
      }
    ]
  }
];

export const PAGES_MENU_A_FORMULARIO = [
  {
    path: 'pages',
    children: [
      {
        path: 'dashboard',
        data: {
          menu: {
            title: 'Dashboard',
            icon: 'ion-android-home',
            selected: false,
            expanded: false,
            order: 0
          }
        }
      },
      {
        path: 'tables',
        data: {
          menu: {
            title: 'general.menu.tables',
            icon: 'ion-happy-outline',
            selected: false,
            expanded: false,
            order: 500,
          }
        }
      },
      {
        path: 'components',
        data: {
          menu: {
            title: 'general.menu.components',
            icon: 'ion-clipboard',
            selected: false,
            expanded: false,
            order: 250,
          }
        },
        children: [
          {
            path: 'treeview',
            data: {
              menu: {
                title: 'general.menu.tree_view',
              }
            }
          },
          {
            path: 'consultarC',
            data: {
              menu: {
                title: 'general.menu.consultarC',
              }
            }
          }
        ]
      },
      {
        path: 'ui',
        data: {
          menu: {
            title: 'general.menu.ui_features',
            icon: 'ion-monitor',
            selected: false,
            expanded: false,
            order: 300,
          }
        }
      },
      {
        path: 'forms',
        data: {
          menu: {
            title: 'general.menu.form_elements',
            icon: 'ion-compose',
            selected: false,
            expanded: false,
            order: 400,
          }
        }
      },
      {
        path: 'charts',
        data: {
          menu: {
            title: 'general.menu.charts',
            icon: 'ion-paintbucket',
            selected: false,
            expanded: false,
            order: 200,
          }
        },
        children: [
          {
            path: 'chartist-js',
            data: {
              menu: {
                title: 'Tema',
              }
            }
          }
        ]   
      }

    ]
  }
];