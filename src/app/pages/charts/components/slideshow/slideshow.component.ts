import {Component,ViewChild} from '@angular/core';
import {SlideShowService} from '../../../../services/slideshow.services'
import {Slide} from '../../../../models/slideshow'
import { LyResizingCroppingImageModule,ResizingCroppingImagesComponent } from 'angular2-resizing-cropping-image';
import {DefaultModalTema} from '../../default-modal/default-modal.component'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'slideshow',
  templateUrl: './slideshow.html',
  styleUrls: ['./slideshow.scss']
})

export class SlideShow {
    public slide: Slide;
    public sliders: Slide[];
    public n: number;
    public limit: number;
    public cantPage: number;
    public filesToUpload: File[];
    @ViewChild(ResizingCroppingImagesComponent) img: ResizingCroppingImagesComponent;
  constructor(
    private _slideSService: SlideShowService,
     private modalService: NgbModal
  ){
    this.n=1;
    this.limit=8;
    this.cantPage=0;
    this.filesToUpload= new Array<File>()
    this.slide= new Slide('','','',[])
    this.sliders = new Array<Slide>()
    this.listSlideshow();
  }
   lgModalShow(slide) {
      const activeModal = this.modalService.open(DefaultModalTema, {size: 'lg'});
      activeModal.componentInstance.modalHeader = 'Eliminar slideshow';
      activeModal.componentInstance.slide = slide;
      //activeModal.componentInstance.show = 'show';
      
  }
  
  next(){
    if(this.n<this.cantPage)
      this.n+=1;
  }
  prev(){
    if(this.n!=1){
      this.n-=1;
    }
  }
  public errorname=false;
  nameError(){
    var style={
      'border-color': this.errorname ? '#FFC107':'#d9dadb'
    }
    return style;
  }
  succesFile(){
    let style={
      'background': this.filesToUpload.length>0 ? '#ffa800':'#00abff'
    }
    return style;
  }
  public crears=false;
  public noimage=true;
  public crear(){
    if(this.slide.imagenes.length<=5){
      if(this.crears){
        this.crears=false;
        this.slide=new Slide('','','',[]);
      }else{
        this.crears=true;
      }
      this.noimage=true;
    }else{
      this.noimage=false;
      this.errormensage='Solo puede guardar entre 3 y 5 imagenes';
    }
    

  }
  public message= 'No hay slideshows creados'
  listSlideshow(){
    this._slideSService.listSlide(JSON.parse(localStorage.getItem('identity')).empresa).subscribe(
      response=>{
        if(response.sliders){
           this.sliders=response.sliders;
           if(this.sliders.length>0){
             this.cantPage = response.total/this.limit;
             this.message='';
           }
        }
      }
      
    )
  }
  public errormensage='';
  public clickguardar=false;
  saveTema(){
    if(this.slide.descripcion!=''){
      this.errorname=false;
      if(this.slide.imagenes.length>=3){
        if(this.slide._id==''){
          this.clickguardar=true;
          this.errormensage='';
          this.slide.empresa=JSON.parse(localStorage.getItem('identity')).empresa;
          this._slideSService.saveTema(this.slide).subscribe(
            response=>{
              if(!response.slideshow){
                console.log(response.message);
              }else{
                this.slide=response.slideshow
                this.crears=false;
                this.clickguardar=false;
                this.slide = new Slide('','','',[]);
                this.errormensage='';
                this.listSlideshow();          
              }
            },
            error=>{
              var errorMessage = <any>error;
                    if(errorMessage != null){
                      //this.errorMessage = body.message;
                      console.log(error);
                }
            }
          )    
        }else{
          this.clickguardar=true;
          this.errormensage='';
          this._slideSService.updateSlide(this.slide).subscribe(
            response=>{
              if(response.slider){
                this.crears=false;
                this.clickguardar=false;
                this.slide = new Slide('','','',[]);
                this.errormensage='';
                this.listSlideshow();
              }
            }
          )
        }
      }else{
        this.errormensage='Debe cargar al menos 3 imagenes para guardar';
      }
    }else{
      this.errormensage='Debe darle un nombre al slideshow';
      this.errorname=true;
    }
  }
  
  
  closeSlide(){
      this.editslid=false;
      this.slide = new Slide('','','',[]);
      this.crears=false;
      this.errormensage='';
      this.listSlideshow();
    
  }
  deleteSlide2(slide){
    this._slideSService.deleteSlide(slide._id).subscribe(
      response=>{
        if(response.slider){
          this.listSlideshow();
        }
      })
  }
  
  fileChangeEvent(file){
    
    if(this.slide.imagenes)
      this.slide.imagenes.push(file);
    else
      this.slide.imagenes[0]=file;

      if(this.slide.imagenes.length<5){
        this.noimage=true
      }else{
        this.noimage=false;
      }
    
  }
  deleteimage(image){
   var n= this.slide.imagenes.indexOf(image);
   this.slide.imagenes.splice(n,1);   
   this.noimage=true;
   
  }
  public selectedImage='';
  setSelectedImage(image){
      this.selectedImage= image;  
  }
  setSelectedImage2(imagenes){
    this.selectedImage=imagenes[0];
    this.arrayimages=imagenes;
  }
  public arrayimages;
  navigate(forward){
    var index;
    if(!this.arrayimages){
      index = this.slide.imagenes.indexOf(this.selectedImage)+(forward ? 1: -1);
      if(index >= 0 && index < this.slide.imagenes.length){
          this.selectedImage = this.slide.imagenes[index];  
      }
    }else{
      index = this.arrayimages.indexOf(this.selectedImage)+(forward ? 1: -1);
      if(index >= 0 && index < this.arrayimages.length){
          this.selectedImage = this.arrayimages[index];  
      }
    }
  }
  public editslid=false;
  edit(slide){
    this.slide=slide;
    this.crears=true;
    this.editslid=true;
  }

}