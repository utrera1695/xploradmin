import {Component} from '@angular/core';
import {TemaService} from '../../../../services/tema.services'
import {Tema} from '../../../../models/tema'
import {DefaultModalTema} from '../../default-modal/default-modal.component'
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'chartist-js',
  templateUrl: './chartistJs.html',
  styleUrls: ['./chartistJs.scss']
})

export class ChartistJs {

  public crear: boolean;
  public tema2: Tema;
  public tema: Tema;
  public temas: Tema[];
  public page: number;
  public url:string;
  public restP: number;
  public restF: number;
  public p:number;
  public f:number;
  public n: number;
  public limit: number;
  public cantPage: number;
  public filesToUpload: File[];
  public percent: number;
  public myvar: any;
  public edit:boolean;
  constructor(
    private _temaService: TemaService,
    private modalService: NgbModal
  ){
    this.crear=false;
    this.temas= new Array<Tema>();
    this.tema = new Tema('','','','','','');
    this.tema2 = new Tema('','','','','','');
    this.page= 1;
    this.url = this._temaService.url;
    this.listTemas();
    this.message='No hay temas creados';
    this.restP=this.p=60;
    this.restF=this.f=140;
    this.filesToUpload= new Array<File>()
    this.n=1;
    this.limit=8;
    this.cantPage=0;
    this.percent=0;
    this.myvar=null;
    this.edit=false;
  }

  createTeme(){
    if(this.crear == false){
      this.crear= true;
    }else{
      this.tema = new Tema('','','','','','');
      this.filesToUpload= new Array<File>();
      this.errorCreate='';
      this.crear= false;
    }
  }
  validate(){
    if(this.tema.nombre!=''){
      this.errorname=false;
      if(this.tema.fraseF!=''){
        this.errorfrase=false;
        return true;
      }else{
        this.errorfrase=true;
        return false;
      }
    }else{
      this.errorname=true;
      return false;
    }
  }
  validate2(){
    if(this.tema2.nombre!=''){
      this.errorname=false;
      if(this.tema2.fraseF!=''){
        this.errorfrase=false;
        return true;
      }else{
        this.errorfrase=true;
        return false;
      }
    }else{
      this.errorname=true;
      return false;
    }
  }
  public errorname=false;
  nameError(){
    var style={
      'border-color': this.errorname ? '#FFC107':'#d9dadb'
    }
    return style;
  }
  public errorfrase=false;
  fraseError(){
    var style={
      'border-color': this.errorfrase ? '#FFC107':'#d9dadb'
    }
    return style;
  }
  lgModalShow(tema) {
      const activeModal = this.modalService.open(DefaultModalTema, {size: 'lg'});
      activeModal.componentInstance.modalHeader = 'Eliminar tema';
      activeModal.componentInstance.tema = tema;
      //activeModal.componentInstance.show = 'show';
      
  }
  next(){
    if(this.n<this.cantPage)
      this.n+=1;
  }
  prev(){
    if(this.n!=1){
      this.n-=1;
    }
  }
  frasep(){
    this.p=this.restP-this.tema.fraseP.length;
  }
  frasef(){
    this.f=this.restF-this.tema.fraseF.length;  
  }
  frasePerror(){
    let style={
      'color':this.p<0 ? '#f95372':'#d9dadb'
    }
    return style;
  }
  fraseFerror(){
    let style={
      'color':this.f<0 ? '#f95372':'#d9dadb'
    }
    return style
  }

  public errorCreate:string;
  saveTema(){
    if(this.validate()){
      if(this.filesToUpload.length>0){
        if(this.filesToUpload[0].size<=2000000){
        this._temaService.saveTema(this.tema).subscribe(
          response=>{
            if(!response.tema){
              console.log(response.message);
            }else{
              console.log(response.tema)
              console.log(this.url+'upload/'+response.tema._id)
              this._temaService.makeFileRequest(this.url+'upload/'+response.tema._id,[],this.filesToUpload,'image')
              .then(
                (result)=>{
                  this.errorCreate=  '';  
                  console.log('Se subio la imagen')
                },
                (error)=>{
                  var errorMessage = <any>error;
                        if(errorMessage != null){
                          //this.errorMessage = body.message;
                          console.log(error);
                    }

                }
              );
              //this.crear=false;
              this.tema = new Tema('','','','','','');
              //let win = (window as any);
                //    win.location.reload();
              this.errorfrase=this.errorname=false;
              this.upload()
              
            }
          },
          error=>{
            var errorMessage = <any>error;
                  if(errorMessage != null){
                    //this.errorMessage = body.message;
                    console.log(error);
              }
          }

        )
        }else{
          this.errorCreate='Imagen demasiado pesada, debe subir una menor a 2mb'
        }
       }else{
          this.errorCreate='Debe subir una imagen de tipo .png o .PNG';
   
       }
    }else{
      this.errorCreate='Debe rellenar todos los campos antes de guardar';
    }
  }
  public message:string;
  listTemas(){
    this._temaService.listTema(JSON.parse(localStorage.getItem('identity')).empresa).subscribe(
      response=>{
        if(!response.temas){
          this.message='No hay temas que mostrar';

        }else{          
          this.temas=response.temas;          
          
          if(this.temas.length>0){
            this.filesToUpload = new Array<File>()
            this.message='';
            this.cantPage = response.total/this.limit;
          }
        }
      },
      error=>{
        var errorMessage = <any>error;
              if(errorMessage != null){
                //this.errorMessage = body.message;
                console.log(error);
          }

      }
    )
  }
  succesFile(){
    let style={
      'background': this.filesToUpload.length>0 ? '#ffa800':'#00abff'
    }
    return style;
  }
  public size;
  public time=0;
  public time2= 0;
  public time3=0;
  fileChangeEvent(fileInput: any){
    this.filesToUpload = <Array<File>>fileInput.target.files;
    this.size=this.filesToUpload[0].size;
    this.time=((this.size/1000)/35)*1000;
      var x = this.time.toString();
      this.time2= parseInt(x)*0.01;
      this.time3=parseInt(x);
    console.log(this.filesToUpload[0])

  }

  upload(){
     var timerId,percent=1;
     var $progress = $('.progress');
     var $progressBar = $('.progress-bar');
     var stringtime;
     $progressBar.css('width', '10%');
     setTimeout(()=>{
       this.listTemas();
       this.crear=false;
     },this.time3)
     timerId = setInterval(function() {
      // increment progress bar
      percent += 1;
      this.percent=percent;
      $progressBar.css('width',percent+'%')
      
      // complete
      if (percent >= 100) {
        clearInterval(timerId); // do more ...
        
        percent=0;
      }
    }, this.time2);
  }
  setTematoedit(tema){
    this.tema2=tema;
    this.edit=true;

  }
  closeEdit(){
    this.tema2=new Tema('','','','','','');
    this.edit=false; 
    this.listTemas();
  }
  update(){
    if(this.validate2()){
        this._temaService.update(this.tema2).subscribe(
          response=>{
            if(!response.tema){
              console.log(response.message);
            }else{
               this.tema2=new Tema('','','','','','');
                this.edit=false;
                this.errorfrase=this.errorname=false;
               this.listTemas();
            }
          },
          error=>{
            var errorMessage = <any>error;
                  if(errorMessage != null){
                    //this.errorMessage = body.message;
                    console.log(error);
              }
          }

        )
    }else{
      this.errorCreate='Debe rellenar todos los campos antes de guardar';
    }
  }

}
