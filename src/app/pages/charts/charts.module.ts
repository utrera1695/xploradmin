import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';

import { routing }       from './charts.routing';
import { Charts } from './charts.component';
import { ChartistJs } from './components/chartistJs/chartistJs.component';
import { SlideShow } from './components/slideshow/slideshow.component'
import { AppTranslationModule } from '../../app.translation.module';
import {TemaService} from '../../services/tema.services'
import {SlideShowService} from '../../services/slideshow.services'

import {AuthGuard} from '../../guards/index';
import {DefaultModalTema} from './default-modal/default-modal.component'
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { LyResizingCroppingImageModule,ResizingCroppingImagesComponent } from 'angular2-resizing-cropping-image';



@NgModule({
  imports: [
    CommonModule,
    AppTranslationModule,
    FormsModule,
    NgaModule,
    routing,
    LyResizingCroppingImageModule,
    NgbModalModule
  ],
  declarations: [
    Charts,
    ChartistJs,
    SlideShow,
    DefaultModalTema
  ],
  providers: [
    TemaService,
    SlideShowService,
    AuthGuard
  ],
  entryComponents: [
    DefaultModalTema    
  ],
})
export class ChartsModule {}
