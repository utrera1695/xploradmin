import { Routes, RouterModule }  from '@angular/router';

import { Charts } from './charts.component';
import { ChartistJs } from './components/chartistJs/chartistJs.component';
import { SlideShow } from './components/slideshow/slideshow.component'
import {AuthGuard} from '../../guards/index';
// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: Charts,
    children: [
      { path: 'chartist-js', component: ChartistJs },
      { path: 'slideshow', component: SlideShow}
    ],
    canActivate: [AuthGuard]
  }
];

export const routing = RouterModule.forChild(routes);
