import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {Tema} from '../../../models/tema'
import {TemaService} from '../../../services/tema.services'
import {SlideShowService} from '../../../services/slideshow.services'
import {Slide} from '../../../models/slideshow'

@Component({
  selector: 'add-service-modal',
  styleUrls: [('./default-modal.component.scss')],
  templateUrl: './default-modal.component.html'
})

export class DefaultModalTema implements OnInit {

  modalHeader: string;
  modalContent: string;
  show: string;
  public tema: Tema;
  public slide: Slide;
  constructor(
    private activeModal: NgbActiveModal,
    private _temaService: TemaService,
    private _slideSService: SlideShowService
    ){
    this.show='';
   
  }

  ngOnInit() {
    
  }

  closeModal() {
    this.activeModal.close();
  }
  
  delete(){
    if(!this.slide){
      this._temaService.deleteTema(this.tema._id).subscribe(
        response=>{
          if(response.tema){
            let win = (window as any);
            win.location.reload();
          }
        },
        error=>{

        }
      )
    }else{
      this._slideSService.deleteSlide(this.slide._id).subscribe(
      response=>{
        if(response.slider){
          this.slide = new Slide('','','',[]);
          let win = (window as any);
          win.location.reload();
        }
      })
    }
  }

}
