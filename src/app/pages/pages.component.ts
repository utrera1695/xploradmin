import { Component } from '@angular/core';
import { Routes } from '@angular/router';

import { BaMenuService } from '../theme';
import {PAGES_MENU,PAGES_MENU_A_DIRECTORIO,PAGES_MENU_A_FORMULARIO,PAGES_MENU_A_MULTIPLE} from './pages.menu';
import {Admin} from '../models/admin'
import {EmpresaService} from '../services/empresa.services'
@Component({
  selector: 'pages',
  template: `
    <ba-sidebar></ba-sidebar>
    <ba-page-top></ba-page-top>
    <div class="al-main">
      <div class="al-content">
        <ba-content-top></ba-content-top>
        <router-outlet></router-outlet>
      </div>
    </div>
    <footer class="al-footer clearfix">      
    </footer>
    
    `
})
export class Pages {
  public identity:Admin;
  constructor(
    private _menuService: BaMenuService,
    private _empresaService: EmpresaService
  ) {
    this.identity= JSON.parse(localStorage.getItem('identity'));
  }
  ngOnInit() {
    //buscar local stoorage el tipo de admin
    
    if(this.identity.adminrol == 'SA')
      this._menuService.updateMenuByRoutes(<Routes>PAGES_MENU);
    else{
      this._empresaService.getEmpresa(this.identity.empresa).subscribe(
        response=>{
          if(response.empresa){
            switch (response.empresa.tipoK) {
              case "Formulario":
                this._menuService.updateMenuByRoutes(<Routes>PAGES_MENU_A_FORMULARIO);
                break;
              case "Directorio":
                this._menuService.updateMenuByRoutes(<Routes>PAGES_MENU_A_DIRECTORIO);
                break;
              case "Multiple":
                this._menuService.updateMenuByRoutes(<Routes>PAGES_MENU_A_MULTIPLE);
                break;
            }
          
          }
        }
      )
    }
  }   
}
