import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule as AngularFormsModule } from '@angular/forms';
import { AppTranslationModule } from '../../app.translation.module';
import { NgaModule } from '../../theme/nga.module';
import { NgbRatingModule } from '@ng-bootstrap/ng-bootstrap';

import { routing }       from './forms.routing';

import { Forms } from './forms.component';
import { Inputs } from './components/inputs';
import { Layouts } from './components/layouts';

import { StandardInputs } from './components/inputs/components/standardInputs';
import { ValidationInputs } from './components/inputs/components/validationInputs';
import { GroupInputs } from './components/inputs/components/groupInputs';
import { CheckboxInputs } from './components/inputs/components/checkboxInputs';
import { Rating } from './components/inputs/components/ratinginputs';
import { SelectInputs } from './components/inputs/components/selectInputs';

import { InlineForm } from './components/layouts/components/inlineForm';
import { BlockForm } from './components/layouts/components/blockForm';
import { HorizontalForm } from './components/layouts/components/horizontalForm';
import { BasicForm } from './components/layouts/components/basicForm';
import { WithoutLabelsForm } from './components/layouts/components/withoutLabelsForm';
import { ReporteCService } from '../../services/reportec.services'
import { DefaultModalR } from './default-modal/default-modal.component';
import {DataTableUFilterPipe} from './default-modal/dataTableU.pipe'
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';

import {CuestionarioService} from '../../services/cuestionario.services'
import {KioskoService} from '../../services/kiosko.services'
import {PreguntaService} from '../../services/pregunta.services'
import {ReporteService} from '../../services/reporte.services'
import {UserService} from '../../services/user.services'

import {TemaService} from '../../services/tema.services'


@NgModule({
  imports: [
    CommonModule,
    AngularFormsModule,
    AppTranslationModule,
    NgaModule,
    NgbRatingModule,
    routing,
    NgbModalModule
  ],
  declarations: [
    Layouts,
    Inputs,
    Forms,
    StandardInputs,
    ValidationInputs,
    GroupInputs,
    CheckboxInputs,
    Rating,
    SelectInputs,
    InlineForm,
    BlockForm,
    HorizontalForm,
    BasicForm,
    WithoutLabelsForm,
    DefaultModalR,
    DataTableUFilterPipe
  ],
  providers:[
      ReporteCService,
      CuestionarioService,
      PreguntaService,
      UserService,
      TemaService,
      KioskoService,
      PreguntaService,
      ReporteService
  ],
  entryComponents:[
      DefaultModalR
  ]
})
export class FormsModule {
}
