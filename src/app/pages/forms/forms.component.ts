import {Component} from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DefaultModalR } from './default-modal/default-modal.component';
import { ReporteCService } from '../../services/reportec.services'
import { ReporteC } from '../../models/reportec'
@Component({
  selector: 'forms',
  templateUrl: './reporte.html',
  styleUrls: ['./reporte.scss']

})
export class Forms {
  public reportesC: ReporteC[];
  public messageR='No hay reportes creados';
  public n=1;
  public limit=10;
  public cantPage=0;
  constructor(
  	private modalService: NgbModal,
  	private _reportecService: ReporteCService
  ){
  	this.reportesC = new Array<ReporteC>();
  	this.listReporteC();
  }
  	lgModalShowReporte(reporte){
      const activeModal = this.modalService.open(DefaultModalR, {size: 'lg'});
      activeModal.componentInstance.reportec = reporte;
      activeModal.componentInstance.modalHeader = 'Reportes del cuestionario "'+reporte.cuestionarioN+'"';
      activeModal.componentInstance.show = 'reporte';
       
    }
    lgModalShowPersonReporte(reporte){
      const activeModal = this.modalService.open(DefaultModalR, {size: 'lg'});
      activeModal.componentInstance.reportec = reporte;
      activeModal.componentInstance.modalHeader = 'Personas registradas en el cuestionario "'+reporte.cuestionarioN+'"';
      activeModal.componentInstance.show = 'reporteperson';
    }
    lgModalShowElim(reporte){
      const activeModal = this.modalService.open(DefaultModalR, {size: 'lg'});
      activeModal.componentInstance.reportec = reporte;
      activeModal.componentInstance.modalHeader = 'Eliminar reporte del cuestionario: "'+reporte.cuestionarioN+'"';
      activeModal.componentInstance.show = 'eliminar';
    }
  listReporteC(){
      this._reportecService.listReporteC(JSON.parse(localStorage.getItem('identity')).empresa).subscribe(
        response=>{
          if(!response.reportc){
            //this.reporteCerror = 'No hay Reporetes'
          }else{
            this.reportesC = response.reportc
            if(this.reportesC.length>0){
              this.messageR='';
              this.cantPage=this.reportesC.length/this.limit;
            }
          }
        },
        error=>{
          var errorMessage = <any>error;
              if(errorMessage != null){
                //this.errorMessage = body.message;
                console.log(error);
          }
        }
      )
    }
    next(){
    if(this.n<this.cantPage)
      this.n+=1;
  }
  prev(){
    if(this.n!=1){
      this.n-=1;
    }
  }
}
