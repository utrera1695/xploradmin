import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import {Cuestionario} from '../../../models/cuestionario'
import {CuestionarioService} from '../../../services/cuestionario.services'
import {Kiosko} from '../../../models/kiosko'
import {KioskoService} from '../../../services/kiosko.services'
import {PreguntaService} from '../../../services/pregunta.services'
import {Pregunta} from '../../../models/pregunta'
import {ReporteService} from '../../../services/reporte.services'
import {Reporte} from '../../../models/reporte'
import {User} from '../../../models/user'
import {UserService} from '../../../services/user.services'
import {ReporteC} from '../../../models/reportec'
import {ReporteCService} from '../../../services/reportec.services'
import {TemaService} from '../../../services/tema.services'
import {Tema} from '../../../models/tema'

declare let jsPDF;
@Component({
  selector: 'add-service-modal',
  styleUrls: [('./default-modal.component.scss')],
  templateUrl: './default-modal.component.html'
})

export class DefaultModalR implements OnInit {
  filter: User = new User('','','','','','','','','','');
  modalHeader: string;
  modalContent: string;
  show: string;
  cuestionarios: Cuestionario[];
  public reportec: ReporteC;
  public kiosko: Kiosko;
  public preguntas: Pregunta[];
  public reporte: Reporte[];
  public user: User[];
  public total: number;
  public s: boolean;
  public sexoS: string;
  public ciudadS: string;
  public acept: boolean;
  public elim: boolean;
  public temas: Tema[];
  public errorK: string;
  public message= 'No se han registrado personas';
  public ftemas: number;
  public fcuest: number;

  constructor(
    private activeModal: NgbActiveModal,
    private _cuestionarioService: CuestionarioService,
    private _kioskoService: KioskoService,
    private _preguntaService: PreguntaService,
    private _reporteService: ReporteService,    
    private _userService: UserService,
    private _reportecService: ReporteCService,
    private _temaService: TemaService
    ){
    this.kiosko = new Kiosko('','','',false,'','',0,'');
    this.reporte= new Array<Reporte>()
    this.preguntas = new Array<Pregunta>();
    this.listCuestionarios();
    this.listTemas();
    this.reportec = new ReporteC('','','','','','','','','','','','','','');
    this.user= new Array<User>();
    this.s=false;
    this.sexoS='Selecione...';
    this.ciudadS='Selecione...';
    this.elim=true;
    this.temas= new Array<Tema>();
    this.errorK= '';
    this.ftemas=0;
    this.fcuest=0;
  }

  ngOnInit() {
    if(this.show == 'true'){
      this.listPregunta();

    }
    if(this.show == 'crear'){
       this.makeid(); 
    }
    if(this.show == 'reporte'){
      this.getReportes();
      if(this.reporte.length>0){
        //this.getData();
      }
    }
    if(this.show == 'reporteperson'){
      this.listUsers();
    }
    if(this.show == 'eliminar'){
      this.getCuestionario();
    }
  }
  listTemas(){
    this._temaService.listTema(JSON.parse(localStorage.getItem('identity')).empresa).subscribe(
      response=>{
        if(!response.temas){
          //this.message=response.message;
          this.ftemas=0;
        }else{
          this.temas=response.temas;
          this.ftemas=this.temas.length;
        }
      },
      error=>{
        var errorMessage = <any>error;
              if(errorMessage != null){
                //this.errorMessage = body.message;
                console.log(error);
          }

      }
    )
  }

  create(){
    if(this.total!=0){
     //Creamos un Elemento Temporal en forma de enlace
        var tmpElemento = document.createElement('a');
        // obtenemos la información desde el div que lo contiene en el html
        // Obtenemos la información de la tabla
        var data_type = 'data:application/vnd.ms-excel; charset=utf-8';
        var tabla_div = document.getElementById('datatable');
        var tabla_html = tabla_div.outerHTML.replace(/ /g, '%20');
        while (tabla_html.indexOf('á') != -1) tabla_html = tabla_html.replace('á', '&aacute;');
        while (tabla_html.indexOf('é') != -1) tabla_html = tabla_html.replace('é', '&eacute;');
        while (tabla_html.indexOf('í') != -1) tabla_html = tabla_html.replace('í', '&iacute;');
        while (tabla_html.indexOf('ó') != -1) tabla_html = tabla_html.replace('ó', '&oacute;');
        while (tabla_html.indexOf('ú') != -1) tabla_html = tabla_html.replace('ú', '&uacute;');
        while (tabla_html.indexOf('º') != -1) tabla_html = tabla_html.replace('º', '&ordm;');
        while (tabla_html.indexOf('ñ') != -1) tabla_html = tabla_html.replace('ñ', '&ntilde;');
        
        tmpElemento.href = data_type + ', ' + tabla_html;
        //Asignamos el nombre a nuestro EXCEL
        tmpElemento.download = 'listadodepersonas.xls';
        // Simulamos el click al elemento creado para descargarlo
        tmpElemento.click();
    }
  }

  closeModal() {
    this.activeModal.close();
    this.kiosko = new Kiosko('','','',false,'','',0,'');
    
  }
  search(){
    if(!this.s){
      this.s= true;
    }else{
      this.s=false;
    }
  }
  makeid(){
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 7; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    this.kiosko.id=text;
  }
  deleteKiosko(){
      this._kioskoService.delete(this.kiosko._id).subscribe(
        response=>{
          if(!response.kiosko){
            alert(response);
          }else{
            let win = (window as any);
            win.location.reload();
            //this.listKiosko();
          }
        },
        error=>{
          var errorMessage = <any>error;
              if(errorMessage != null){
                //this.errorMessage = body.message;
                console.log(error);
          }
        }
      )
  }
  save(){
    if(this.kiosko._id == ''){
      if((this.kiosko.nombre != '')&&(this.kiosko.id != '')){
        if((this.kiosko.tema!='')&&(this.kiosko.cuestionario!='')){
          this._kioskoService.save(this.kiosko).subscribe(
            response=>{
              if(!response.kiosko){
                 alert('No se pudo crear el kiosko');
              }else{
                this.activeModal.close();
                this.errorK='';
                this.kiosko = new Kiosko('','','',false,'','',0,'');
                let win = (window as any);
                win.location.reload();
              }
            },
            error=>{
              var errorMessage = <any>error;
              if(errorMessage != null){
                //this.errorMessage = body.message;
                console.log(error);
              }
            }
          )
        }else{
          this.errorK='Debe seleccionar un tema y un cuestionario';
        }
      }else{
        this.errorK='Debe darle nombre al kiosko';
      }
  }else{
    if((this.kiosko.nombre != '')&&(this.kiosko.id != '')){
      if((this.kiosko.tema!='')&&(this.kiosko.cuestionario!='')){
          this._kioskoService.update(this.kiosko).subscribe(
              response=>{
                if(!response.kiosko){
                  alert(response.message);
                }else{
                  this.closeModal();
                }
              },
              error=>{
                var errorMessage = <any>error;
                if(errorMessage != null){
                  //this.errorMessage = body.message;
                  console.log(error);
                }
              }
            )
          }else{
            this.message='Debe seleccionar un tema y un cuestionario';
          }       
    }else{
      this.errorK='Debe darle nombre al kiosko';
    }
  }
}
getCuestionario(){
  this._cuestionarioService.get(this.reportec.cuestionario).subscribe(
    response=>{
      if(!response.cuestion){
        this.elim = true;
        //console.log(response.message);
      }else{
        this.elim= false;
      }
    },
    error=>{
      var errorMessage = <any>error;
        if(errorMessage != null){
        //this.errorMessage = body.message;
        console.log(error);
        }

    }
  )
}
DeleteReporteC(value){
  if(value == 'si'){
    this._reportecService.delete(this.reportec._id).subscribe(
        response=>{
          if(!response.reportesc){
            alert(response.message)
          }else{
            let win = (window as any);
            win.location.reload();
            this.closeModal();
          }
        },
        error=>{
          var errorMessage = <any>error;
            if(errorMessage != null){
              //this.errorMessage = body.message;
              console.log(error);
            }
        }
    );
  }else{
    this.closeModal();
  }
}

  listCuestionarios(){
    this._cuestionarioService.listCuestionario(JSON.parse(localStorage.getItem('identity')).empresa).subscribe(
        response =>{
          if(!response.cuestion){
            this.fcuest=0;
            console.log(response);
          }else{
            this.cuestionarios = response.cuestion;
            this.fcuest=this.cuestionarios.length;
            
          }
            
          },
          error=>{
            var errorMessage = <any>error;
            if(errorMessage != null){
              //this.errorMessage = body.message;
              console.log(error);
             }
          }
      );
  };
  listPregunta(){
    this._preguntaService.listPreguntas(this.kiosko.cuestionario).subscribe(
        response =>{
          if(!response.pregunta){
            alert('No se pudieron cargar las preguntas');
          }else{
            this.preguntas = response.pregunta;
          }
        },
        error =>{
          var errorMessage = <any>error;
            if(errorMessage != null){
              console.log(error);
            }
        }
      );
  }

  getReportes(){
    this._reporteService.listReport(this.reportec.cuestionario).subscribe(
      response=>{
        if(!response.reportes){
          alert(response.message);
        }else{
          this.reporte=response.reportes;
          this.total=response.total;
          this.listUsers();
        }
      },
      error=>{
        var errorMessage = <any>error;
          if(errorMessage != null){
            //this.errorMessage = body.message;
            console.log(error);
        }        
      }
    )
  }
  clean(){
    this.ciudadS='Selecione...';
    this.sexoS='Selecione...';
    this.listUsers();
  }
  public showfilt=false;
  filt(){
    if(this.showfilt)
      this.showfilt=false;
    else
      this.showfilt=true;
  }
  public person=false;
  public usertotal = 0;
  listUsers(){
    this._userService.list(this.reportec.cuestionario).subscribe(
        response=>{
          if(!response.user){
            this.user = new Array<User>();
            console.log(response.message)
          }else{
            this.user = response.user;
            if(this.show=='reporte'){
              this.usertotal= response.total
              this.getData();
            }
            if(this.user.length>0){
              this.person=true;
              this.message='';
              this.cantPage = this.user.length/this.limit;
            }
          }
        },
        error=>{
           var errorMessage = <any>error;
            if(errorMessage != null){
              //this.errorMessage = body.message;
              console.log(error);
          } 
        }
    )
  }

  public sexo= new Array<number>();
  public edad= new Array<number>();
  
  getData(){
    this.sexo[0]=this.sexo[3]=0;//MASCULINO
    this.sexo[1]=this.sexo[4]=0;//FEMENINO
    
    this.edad[0]=this.edad[5]=0;//DE 18 A 29
    this.edad[1]=this.edad[6]=0;//DE 30 A 39
    this.edad[2]=this.edad[7]=0;//DE 40 A 49
    this.edad[3]=this.edad[8]=0;//DE 50 A 59
    this.edad[4]=this.edad[9]=0;//DE 60 EN ADELANTE
    
    for(var i=0;i<this.reporte.length;i++){
      for(var j=0;j<this.user.length;j++){
        if(this.user[j]._id == this.reporte[i].user){
          //por sexo
          if(this.user[j].sexo =='M'){
            this.sexo[0]+=1;
          }
          if(this.user[j].sexo == 'F'){
            this.sexo[1]+=1;
          }
          //por rango de edad
          if((parseInt(this.user[j].edad)>=18)&&(parseInt(this.user[j].edad)<30)){
            this.edad[0]+=1;
          }
          if((parseInt(this.user[j].edad)>=30)&&(parseInt(this.user[j].edad)<40)){
            this.edad[1]+=1;
          }
          if((parseInt(this.user[j].edad)>=40)&&(parseInt(this.user[j].edad)<50)){
            this.edad[2]+=1;
          }
          if((parseInt(this.user[j].edad)>=50)&&(parseInt(this.user[j].edad)<60)){
            this.edad[3]+=1;
          }
          if(parseInt(this.user[j].edad)>=60){
            this.edad[4]+=1;
          }
        }
      }
    }
    if(this.total !=0){
        this.sexo[3]=parseFloat(Number((this.sexo[0]*100)/this.total).toFixed(2));
        this.sexo[4]=parseFloat(Number((this.sexo[1]*100)/this.total).toFixed(2));
        this.edad[5]=parseFloat(Number((this.edad[0]*100)/this.total).toFixed(2));
        this.edad[6]=parseFloat(Number((this.edad[1]*100)/this.total).toFixed(2));
        this.edad[7]=parseFloat(Number((this.edad[2]*100)/this.total).toFixed(2));
        this.edad[8]=parseFloat(Number((this.edad[3]*100)/this.total).toFixed(2));
        this.edad[9]=parseFloat(Number((this.edad[4]*100)/this.total).toFixed(2));
    }
  }

  createPdf(){
    if(this.total!=0){
      var doc = new jsPDF();
        doc.setFontSize(14);
        doc.text(20, 20,this.reportec.cuestionarioN);
        doc.setFontSize(12);
        doc.text(20,25,'Reporte General')
        doc.text(20, 40, 'Total de Registros Realizados: '+this.total)
        doc.setFontSize(11);
        doc.text(20,50,'Por Sexo');
        doc.setFontSize(8);
        doc.text(25,55,'Masculino: '+this.sexo[0]+' - '+this.sexo[3]+'%');
        doc.text(25,60,'Femenino: '+this.sexo[1]+' - '+this.sexo[4]+'%');
        doc.setFontSize(11);
        doc.text(20,70,'Por Edad');      
        doc.setFontSize(8);
        doc.text(25,75,'Entre 18 y 29: '+this.edad[0]+' - '+this.edad[5]+'%');
        doc.text(25,80,'Entre 30 y 39: '+this.edad[1]+' - '+this.edad[6]+'%');
        doc.text(25,85,'Entre 40 y 49: '+this.edad[2]+' - '+this.edad[7]+'%');
        doc.text(25,90,'Entre 50 y 59: '+this.edad[3]+' - '+this.edad[8]+'%');
        doc.text(25,95,'Mas de 60    : '+this.edad[4]+' - '+this.edad[9]+'%');
                

        // Save the PDF
        doc.save(this.reportec.cuestionarioN+'.pdf');
    }
  }
  public n=1;
  public limit =15;
  public cantPage=0;
  next(){
    if(this.n<this.cantPage)
      this.n+=1;
  }
  prev(){
    if(this.n!=1){
      this.n-=1;
    }
  }
}
