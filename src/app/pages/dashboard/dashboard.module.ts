import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { AppTranslationModule } from '../../app.translation.module';
import { NgaModule } from '../../theme/nga.module';
import { NgbDropdownModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';

import { Dashboard } from './dashboard.component';
import { routing }       from './dashboard.routing';

import { PopularApp } from './popularApp';
import { PieChart } from './pieChart';

import { UsersMap } from './usersMap';
import { Calendar } from './calendar';
import { CalendarService } from './calendar/calendar.service';
import { TodoService } from './todo/todo.service';
import { TrafficChartService } from './trafficChart/trafficChart.service';
import { UsersMapService } from './usersMap/usersMap.service';
import { PieChartService } from './pieChart/pieChart.service';
import { DefaultModalDash } from './default-modal/default-modal.component';

import {CuestionarioService} from '../../services/cuestionario.services'
import {KioskoService} from '../../services/kiosko.services'
import {PreguntaService} from '../../services/pregunta.services'
import {ReporteCService} from '../../services/reportec.services'
import {ReporteService} from '../../services/reporte.services'
import {UserService} from '../../services/user.services'
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import {AuthGuard} from '../../guards/index';
import {TemaService} from '../../services/tema.services'
import {DataTableUFilterPipe} from './default-modal/dataTableU.pipe'
import {EmpresaService} from '../../services/empresa.services'
import {KioskoDService} from '../../services/kioskod.services'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AppTranslationModule,
    NgaModule,
    NgbModalModule,
    routing,
    HttpModule
  ],
  declarations: [
    PopularApp,
    PieChart,
    Calendar,
    DefaultModalDash,
    Dashboard,    
    DataTableUFilterPipe
    ],
  providers: [
    CalendarService,
    TodoService,
    TrafficChartService,
    UsersMapService,
    CuestionarioService,
    PreguntaService,
    EmpresaService,
    KioskoService,
    KioskoDService,
    ReporteService,
    ReporteCService,
    UserService,
    PieChartService,
    TemaService,
    AuthGuard
  ],
  entryComponents: [
    DefaultModalDash    
  ],
})
export class DashboardModule {}
