import {Component, OnChanges} from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DefaultModalDash } from './default-modal/default-modal.component';
import {Kiosko} from '../../models/kiosko'
import {KioskoService} from '../../services/kiosko.services'
import {ReporteC} from '../../models/reportec'
import {ReporteCService} from '../../services/reportec.services'
import {EmpresaService} from '../../services/empresa.services'
import {Empresa} from '../../models/empresa'
import {KioskoDService} from '../../services/kioskod.services'
import {KioskoD} from '../../models/kioskoD'

@Component({
  selector: 'dashboard',
  styleUrls: ['./dashboard.scss'],
  templateUrl: './dashboard.html'
})
export class Dashboard  implements OnChanges{

	public kioskos : Kiosko[];
  public kioskosD: KioskoD[];
	public activo: boolean;
	public reportesC: ReporteC[];
  public messageR: string;
  public messageK: string;
  public empresa: Empresa;
  constructor(
	  	private modalService: NgbModal,
	  	private _kioskoService: KioskoService,
      private _reportecService: ReporteCService,
	    private _empresaService: EmpresaService,
      private _kioskoDService: KioskoDService
  ){
    this.reportesC= new Array<ReporteC>();
		this.kioskos = new Array<Kiosko>();
    this.empresa = new Empresa('','','',0,'')
		this.activo = true;
		this.listKiosko();
    this.listReporteC();
    this.getEmpresa();
    this.active();
    this.messageK= 'No hay kioskos creados';
    this.messageR= 'No hay reportes creados';
    setInterval(()=>{this.active()},15000)

	}
	ngOnChanges(){
		this.listKiosko();	
	}
  	lgModalShow(){
      //crear
    	const activeModal = this.modalService.open(DefaultModalDash, {size: 'lg'});
  		activeModal.componentInstance.modalHeader = "Crear Kiosko";
  		activeModal.componentInstance.show = 'crear';
  		
  	}
  	lgModalShowKioskoEdit(kiosko){
      //Editar
    	const activeModal = this.modalService.open(DefaultModalDash, {size: 'lg'});
    	activeModal.componentInstance.kiosko = kiosko;
  		activeModal.componentInstance.modalHeader = 'Editar Kiosko';
  		activeModal.componentInstance.show = 'false';	
  	}
  	lgModalShowKiosko(kiosko){
      //visualizar
    	const activeModal = this.modalService.open(DefaultModalDash, {size: 'lg'});
    	activeModal.componentInstance.kiosko = kiosko;
  		activeModal.componentInstance.modalHeader = kiosko.nombre+' - ID: '+kiosko.id;
  		activeModal.componentInstance.show = 'true';
  		
  	}
    lgModalShowReporte(reporte){
      const activeModal = this.modalService.open(DefaultModalDash, {size: 'lg'});
      activeModal.componentInstance.reportec = reporte;
      activeModal.componentInstance.modalHeader = 'Reportes del cuestionario "'+reporte.cuestionarioN+'"';
      activeModal.componentInstance.show = 'reporte';
       
    }
    lgModalShowPersonReporte(reporte){
      const activeModal = this.modalService.open(DefaultModalDash, {size: 'lg'});
      activeModal.componentInstance.reportec = reporte;
      activeModal.componentInstance.modalHeader = 'Personas registradas en el cuestionario "'+reporte.cuestionarioN+'"';
      activeModal.componentInstance.show = 'reporteperson';
    }
    lgModalShowElim(reporte){
      const activeModal = this.modalService.open(DefaultModalDash, {size: 'lg'});
      activeModal.componentInstance.reportec = reporte;
      activeModal.componentInstance.modalHeader = 'Eliminar reporte del cuestionario: "'+reporte.cuestionarioN+'"';
      activeModal.componentInstance.show = 'eliminar';
    }
    lgModalKioskoElim(kiosko){
      const activeModal = this.modalService.open(DefaultModalDash, {size: 'lg'});
      activeModal.componentInstance.kiosko = kiosko;
      activeModal.componentInstance.modalHeader = 'Eliminar Kiosko: "'+kiosko.nombre+'"';
      activeModal.componentInstance.show = 'deletek';
    }
    public getEmpresa(){
      this._empresaService.getEmpresa(JSON.parse(localStorage.getItem('identity')).empresa).subscribe(
       response=>{
         if(response.empresa){
           this.empresa=response.empresa
         }
       }
      )
    }
  	listKiosko(){
  		this._kioskoService.listKiosko2(1,JSON.parse(localStorage.getItem('identity')).empresa).subscribe(
  			response=>{
  				if(!response.kioskos){
  					alert(response);
  				}else{
  					this.kioskos=response.kioskos;
            if(this.kioskos.length>0){
              this.messageK='';
            }
  				}
  			},
  			error=>{
  				var errorMessage = <any>error;
	          	if(errorMessage != null){
	            	//this.errorMessage = body.message;
	            	console.log(error);
  				}
  			}
  		)
      this._kioskoDService.listKiosko2(JSON.parse(localStorage.getItem('identity')).empresa,1).subscribe(
        response=>{
          if(!response.kioskos){
            alert(response.message)
          }else{
            this.kioskosD= response.kioskos;            
            if(this.kioskosD.length>0)
              this.messageK='';
          }
        }
      )
  	}
    
    public reporteCerror;
    listReporteC(){
      this._reportecService.listReporteC2(1,JSON.parse(localStorage.getItem('identity')).empresa).subscribe(
        response=>{
          if(!response.reportc){
            //this.reporteCerror = 'No hay Reporetes'
          }else{
            this.reportesC = response.reportc
            if(this.reportesC.length>0){
              this.messageR='';
            }
          }
        },
        error=>{
          var errorMessage = <any>error;
              if(errorMessage != null){
                //this.errorMessage = body.message;
                console.log(error);
          }
        }
      )
    }

    active(){
      for (var i=0;i<this.kioskos.length; ++i){
        this._kioskoService.getKiosko(this.kioskos[i]).subscribe(
          response=>{
            if(response.kiosko){
              var date = new Date();
              var h= date.getHours()*3600;
              var m= date.getMinutes()*60;
              var s= date.getSeconds();
                      
              var dif = (h+m+s)- response.kiosko.time;              
              console.log(dif+' Kiosko:'+response.kiosko.nombre);
              if((dif>35)&&(response.kiosko.estado==true)){
                response.kiosko.estado=false;
                this._kioskoService.update2(response.kiosko).subscribe(
                  response=>{
                    if(!response.kiosko){
                      alert('Error al intentar listar los kioskos activos');
                    }
                  },
                  error=>{
                    var errorMessage = <any>error;
                      if(errorMessage != null){
                        //this.errorMessage = body.message;
                        console.log(error);
                      }
                  }
                );
              }
              if(dif<0){
                response.kiosko.estado=false;
                this._kioskoService.update2(response.kiosko).subscribe(
                  response=>{
                    if(!response.kiosko){
                      alert('Error al intentar listar los kioskos activos');
                    }
                  },
                  error=>{
                    var errorMessage = <any>error;
                      if(errorMessage != null){
                        //this.errorMessage = body.message;
                        console.log(error);
                      }
                  }
                );
              }
            }
          },
          error=>{
            var errorMessage = <any>error;
              if(errorMessage != null){
                //this.errorMessage = body.message;
                console.log(error);
            }
          }
        )
      }      
      this.listKiosko();
    }
}
