import {Injectable} from '@angular/core';
import {BaThemeConfigProvider, colorHelper} from '../../../theme';

@Injectable()
export class PieChartService {

  
  constructor(
    private _baConfig:BaThemeConfigProvider,
    
    ){
  }

  getData() {
    let pieColor = this._baConfig.get().colors.custom.dashboardPieChart;
     
    return [
      {
        color: pieColor,
        description: 'dashboard.new_visits',
        stats: '0',
        icon: 'person',
      }, {
        color: pieColor,
        description: 'dashboard.purchases',
        stats: '0',
        icon: 'money',
      }
    ];
  }
  
}
