import {Component} from '@angular/core';

import {PieChartService} from './pieChart.service';
import {CuestionarioService} from '../../../services/cuestionario.services'
import {UserService} from '../../../services/user.services'
import {PreguntaService} from '../../../services/pregunta.services'
import {ReporteService} from '../../../services/reporte.services'
import 'easy-pie-chart/dist/jquery.easypiechart.js';

@Component({
  selector: 'pie-chart',
  templateUrl: './pieChart.html',
  styleUrls: ['./pieChart.scss']
})
// TODO: move easypiechart to component
export class PieChart {

  public charts: Array<Object>;
  private _init = false;
  public cuestionarioTotal: Number;

  constructor(
    private _cuestionarioService: CuestionarioService,
    private _pieChartService: PieChartService,
    private _userService: UserService,
    private _preguntaService: PreguntaService,
    private _reporteService: ReporteService
    ){
 
    this.charts = this._pieChartService.getData();
    this.setCC();
    this.setPT();
    }

  ngAfterViewInit() {
    if (!this._init) {
      this._loadPieCharts();
      this._updatePieCharts();
      this._init = true;
    }
  }

  private _loadPieCharts() {

    jQuery('.chart').each(function () {
      let chart = jQuery(this);
      chart.easyPieChart({
        easing: 'easeOutBounce',
        onStep: function (from, to, percent) {
          jQuery(this.el).find('.percent').text(20);
        },
        barColor: jQuery(this).attr('data-rel'),
        trackColor: 'rgba(0,0,0,0)',
        size: 84,
        scaleLength: 0,
        animation: 2000,
        lineWidth: 9,
        lineCap: 'round',
      });
    });

  }

  private _updatePieCharts() {
    let getRandomArbitrary = 10;//(min, max) => { return Math.random() * (max - min) + min; };

    jQuery('.pie-charts .chart').each(function(index, chart) {
      jQuery(chart).data('easyPieChart').update(20);
    });
  }

  setCC(){
    this._cuestionarioService.listCuestionario(JSON.parse(localStorage.getItem('identity')).empresa).subscribe(
      response=>{
        if(response.total){
          this.charts[0]= {
                color:'',
                description: 'dashboard.new_visits',
                stats: response.total,
                icon: 'comments',
                percent:'30',
              };
        }
      },
      error=>{

      }
    )
  }
  
  setPT(){
    this._preguntaService.listPreguntas2(JSON.parse(localStorage.getItem('identity')).empresa).subscribe(
      response=>{
        if(response.total){
          this.charts[1]={
            color: '',
            description: 'dashboard.purchases',
            stats: response.total,
            icon: 'ion-help',

          }
        }
      }
    )
  }
  
}
