import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';
import { NgbDropdownModule, NgbModalModule } from '@ng-bootstrap/ng-bootstrap';

import { routing }       from './ui.routing';
import { Ui } from './ui.component';
import { Formulario } from './components/formulario/formulario.component'
import { Directorio } from './components/directorio/directorio.component'
import { Botones } from './components/botones/botones.component'
import { DefaultModal3 } from './default-modal/default-modal.component';
import {KioskoService} from '../../services/kiosko.services'
import {KioskoDService} from '../../services/kioskod.services'
import {EmpresaService} from '../../services/empresa.services'
import {CuestionarioService} from '../../services/cuestionario.services'
import {BotonService} from '../../services/boton.services'
import {PreguntaService} from '../../services/pregunta.services'
import {SlideShowService} from '../../services/slideshow.services'
import {TemaService} from '../../services/tema.services'
import {AuthGuard} from '../../guards/index';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgaModule,
    NgbDropdownModule,
    NgbModalModule,
    routing
  ],
  declarations: [
    Ui,
    Formulario,
    Directorio,
    Botones,
    DefaultModal3

  ],
  entryComponents: [
    DefaultModal3
  ],
  providers: [
    PreguntaService,
    KioskoService,
    KioskoDService,
    CuestionarioService,
    BotonService,
    SlideShowService,
    EmpresaService,
    TemaService,
    AuthGuard
  ]
})
export class UiModule {
}
