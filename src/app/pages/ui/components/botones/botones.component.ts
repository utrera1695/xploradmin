import {Component,Input,Output,EventEmitter} from '@angular/core';
import {KioskoD} from '../../../../models/kioskoD'
import {Boton} from '../../../../models/boton'
import {BotonService} from '../../../../services/boton.services'

@Component({
  selector: 'botones',
  templateUrl: './botones.html',
  styleUrls: ['./botones.scss']
})
export class Botones {
  @Output() Event= new EventEmitter()
  @Input() botones: Boton[];
  public btn: Boton;
  public icons=[
    {value:'ion-arrow-up-a',display:'arrow-up-a'},
    {value:'ion-arrow-shrink',display:'arrow-shrink'},
    {value:'ion-navicon-round',display:'navicon-round'},
    {value:'ion-log-in',display:'log-in'},
    {value:'ion-checkmark-round',display:'checkmark-round'},
    {value:'ion-information',display:'ion-information'},
    {value:'ion-information-circled',display:'ion-information-circled'},
    {value:'ion-help',display:'ion-help'},
    {value:'ion-help-circled',display:'ion-help-circled'},
    {value:'ion-help-buoy',display:'ion-help-buoy'},
    {value:'ion-home',display:'ion-home'},
    {value:'ion-search',display:'ion-search'},
    {value:'ion-flag',display:'ion-flag'},
    {value:'ion-heart',display:'ion-heart'},
    {value:'ion-settings',display:'ion-settings'},
    {value:'ion-trash-a',display:'ion-trash-a'},
    {value:'ion-fork',display:'ion-fork'},
    {value:'ion-knife',display:'ion-knife'},
    {value:'ion-coffee',display:'ion-coffee'},
    {value:'ion-icecream',display:'ion-icecream'},
    {value:'ion-pizza',display:'ion-pizza'},
    {value:'ion-beer',display:'ion-beer'},
    {value:'ion-wifi',display:'ion-wifi'},
    {value:'ion-monitor',display:'ion-monitor'},
    {value:'ion-music-note',display:'ion-music-note'},
    {value:'ion-model-s',display:'ion-model-s'},
    {value:'ion-thermometer',display:'ion-thermometer'},
    {value:'ion-cash',display:'ion-cash'},
    {value:'ion-pricetags',display:'ion-pricetags'},
    {value:'ion-location',display:'ion-location'},
    {value:'ion-medkit',display:'ion-medkit'},
    {value:'ion-cube',display:'ion-cube'},
    {value:'ion-tshirt-outline',display:'ion-tshirt-outline'},
    {value:'ion-leaf',display:'ion-leaf'},
    {value:'ion-woman',display:'ion-arrow-up-a'},
    {value:'ion-man',display:'ion-man'},
    {value:'ion-female',display:'ion-female'},
    {value:'ion-waterdrop',display:'ion-waterdrop'},
    {value:'ion-ribbon-a',display:'ion-ribbon-a'},
    {value:'ion-trophy',display:'ion-trophy'},
    {value:'ion-happy-outline',display:'ion-happy-outline'},
    {value:'ion-bag',display:'ion-bag'},
    {value:'ion-paintbrush',display:'ion-paintbrush'},
    {value:'ion-image',display:'ion-image'}
  ]
  constructor(
    private _botonService: BotonService
  ){
  }

  public close(){
    this.Event.emit();
  }
  public edit=false;
  public editar(boton){
    if(this.edit){
      this.btn=null;
      this.edit=false;
    }else{
      this.btn=boton;
      this.edit=true;
    }
  }
  public cancelar(){
    this.edit=false;
    this.listBotones(this.btn.kiosko)
    this.btn=null;
  }

  public listBotones(kioskoid){
    this._botonService.listbotons(kioskoid).subscribe(
      response=>{
        if(response.boton){
          this.botones=response.boton;
        }
      }
    )
  }

  public updateBtn(){
    this._botonService.update(this.btn).subscribe(
      response=>{
        if(response.boton){
          this.edit=false;
          this.listBotones(this.btn.kiosko);
          this.btn=null;
        }
      }
    )
  }
  public updateBtn2(btn){
    this._botonService.update(btn).subscribe(
      response=>{
        if(response.boton){
          this.edit=false;
          this.listBotones(btn.kiosko);
          this.btn=null;
        }
      }
    )
  }

}