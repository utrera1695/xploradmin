import {Component,Input,EventEmitter,Output} from '@angular/core';
import {Kiosko} from '../../../../models/kiosko'
import {KioskoService} from '../../../../services/kiosko.services'
import {Cuestionario} from '../../../../models/cuestionario'
import {CuestionarioService} from '../../../../services/cuestionario.services'
import {TemaService} from '../../../../services/tema.services'
import {Tema} from '../../../../models/tema'

@Component({
  selector: 'formulario',
  templateUrl: './formulario.html'
})
export class Formulario {
  @Input() kiosko: Kiosko;
  @Output() Event= new EventEmitter()
  public cuestionarios: Cuestionario[]
  public temas: Tema[];
  public errork: string;
  public ftemas: number;
  public fcuest: number;
  
  
  constructor(
  	private _cuestionarioService: CuestionarioService,
  	private _temaService: TemaService,
  	private _kioskoService: KioskoService
  ){
  	this.temas= new Array<Tema>();
	this.cuestionarios= new Array<Cuestionario>();
	this.listCuestionarios();
	this.listTemas();
  }

  public close(){
    this.Event.emit();
  }
  
  makeid(){
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      for( var i=0; i < 7; i++ )
          text += possible.charAt(Math.floor(Math.random() * possible.length));

      this.kiosko.id=text;
    }

  save(){
    this.errorselect=this.errorname=false;
    if(this.kiosko._id == ''){
      if((this.kiosko.nombre != '')&&(this.kiosko.id != '')){
        if((this.kiosko.tema!='')&&(this.kiosko.cuestionario!='')){
          this._kioskoService.save(this.kiosko).subscribe(
            response=>{
              if(!response.kiosko){
                 alert('No se pudo crear el kiosko');
              }else{
                this.errork='';
                this.errorselect=false;
                this.errorname=false;
                this.kiosko = new Kiosko('','','',false,'','',0,JSON.parse(localStorage.getItem('identity')).empresa);                
                this.Event.emit();
                
              }
            },
            error=>{
              var errorMessage = <any>error;
              if(errorMessage != null){
                //this.errorMessage = body.message;
                console.log(error);
              }
            }
          )
        }else{
          this.errorselect=true;
          this.errork='Debe seleccionar un tema y un cuestionario';
        }
      }else{
        this.errorname=true;
        this.errork='Debe darle nombre al kiosko';
      }
  }else{
    if((this.kiosko.nombre != '')&&(this.kiosko.id != '')){
      this._kioskoService.update(this.kiosko).subscribe(
          response=>{
            if(!response.kiosko){
              alert(response.message);
            }else{
              this.errorname=false;
              this.Event.emit();
            }
          },
          error=>{
            var errorMessage = <any>error;
            if(errorMessage != null){
              //this.errorMessage = body.message;
              console.log(error);
            }
          }
        )       
    }else{
      this.errorname=true;
      this.errork='Debe darle nombre al kiosko';
    }
  }
}

public errorname=false;
nameError(){
  var style={
      'border-color': this.errorname ? '#FFC107':'#efefef'
  }
  return style;
}
public errorselect=false;
selectError(){
  var style={
      'border-color': this.errorselect ? '#FFC107':'#efefef'
  }
  return style;
}	
listCuestionarios(){
    this._cuestionarioService.listCuestionario(JSON.parse(localStorage.getItem('identity')).empresa).subscribe(
        response =>{
          if(!response.cuestion){
            console.log(response);
            this.fcuest=0;
          }else{
            this.cuestionarios = response.cuestion;
            this.fcuest=this.cuestionarios.length;
          }
            
          },
          error=>{
            var errorMessage = <any>error;
            if(errorMessage != null){
              //this.errorMessage = body.message;
              console.log(error);
             }
          }
      );
  };
listTemas(){
    this._temaService.listTema(JSON.parse(localStorage.getItem('identity')).empresa).subscribe(
      response=>{
        if(!response.temas){
          //this.message=response.message;
          this.ftemas=0;
        }else{
          this.temas=response.temas;
          this.ftemas=this.temas.length;
        }
      },
      error=>{
        var errorMessage = <any>error;
              if(errorMessage != null){
                //this.errorMessage = body.message;
                console.log(error);
          }

      }
    )
}
}
