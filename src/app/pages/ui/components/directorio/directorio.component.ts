import {Component,Input,Output,EventEmitter} from '@angular/core';
import {KioskoD} from '../../../../models/kioskoD'
import {KioskoDService} from '../../../../services/kioskod.services'
import {Boton} from '../../../../models/boton'
import {BotonService} from '../../../../services/boton.services'
import {SlideShowService} from '../../../../services/slideshow.services'
import {Slide} from '../../../../models/slideshow'

@Component({
  selector: 'directorio',
  templateUrl: './directorio.html'
})
export class Directorio {
  @Input() kiosko: KioskoD;
  @Input() buttons: number;
  @Output() Event= new EventEmitter();
  public sliders: Slide[];
  constructor(
    private _kioskoDService: KioskoDService,
    private _botonService: BotonService,
    private _slideSService: SlideShowService
  ){
    this.listSlideshow()
  }

  public close(){
    this.Event.emit();
  }
  
  makeid(){
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      for( var i=0; i < 7; i++ )
          text += possible.charAt(Math.floor(Math.random() * possible.length));

      this.kiosko.id=text;
    }
  public errorname=false;
  nameError(){
    var style={
        'border-color': this.errorname ? '#FFC107':'#efefef'
    }
    return style;
  }
  public errorselect=false;
  selectError(){
    var style={
        'border-color': this.errorselect ? '#FFC107':'#efefef'
    }
    return style;
  }

  public errork='';
  save(){
    this.errorselect=this.errorname=false;
      if(this.kiosko._id == ''){
        if((this.kiosko.descripcion != '')&&(this.kiosko.id != '')){
            this._kioskoDService.save(this.kiosko).subscribe(
              response=>{
                if(!response.kioskoD){
                   alert('No se pudo crear el kiosko');
                }else{
                  this.errork='';
                  this.errorselect=false;
                  this.errorname=false;
                  for(var i=0;i<this.buttons;i++){
                    var boton:Boton;
                    console.log(i)
                    boton = new Boton('','Boton '+(i+1),false,'','ion-cube',response.kioskoD._id)
                    this._botonService.save(boton).subscribe(
                      response=>{
                        if(!response.boton){
                          alert(response.message);
                        }
                      }
                    );

                  }
                  this.kiosko = new KioskoD('','','',0,false,'','');                
                  this.Event.emit();
                  
                }
              },
              error=>{
                var errorMessage = <any>error;
                if(errorMessage != null){
                  //this.errorMessage = body.message;
                  console.log(error);
                }
              }
            )
          
        }else{
          this.errorname=true;
          this.errork='Debe darle nombre al kiosko';
        }
    }else{
      if((this.kiosko.descripcion != '')&&(this.kiosko.id != '')){
        this._kioskoDService.update(this.kiosko).subscribe(
            response=>{
              if(!response.kioskoD){
                alert(response.message);
              }else{
                this.errorname=false;
                this.Event.emit();
              }
            },
            error=>{
              var errorMessage = <any>error;
              if(errorMessage != null){
                //this.errorMessage = body.message;
                console.log(error);
              }
            }
          )       
      }else{
        this.errorname=true;
        this.errork='Debe darle nombre al kiosko';
      }
    }
  }
   public total=0;
   listSlideshow(){
    this._slideSService.listSlide(JSON.parse(localStorage.getItem('identity')).empresa).subscribe(
      response=>{
        if(response.sliders){
           this.sliders=response.sliders;
           if(this.sliders.length>0){
             this.total = response.total;
           }
        }
      }
      
    )
  }
}