import {Component} from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DefaultModal3} from './default-modal/default-modal.component';
import {Kiosko} from '../../models/kiosko'
import {KioskoService} from '../../services/kiosko.services'
import {EmpresaService} from '../../services/empresa.services'
import {KioskoD} from '../../models/kioskoD'
import {KioskoDService} from '../../services/kioskod.services'
import {Boton} from '../../models/boton'
import {BotonService} from '../../services/boton.services'

@Component({
  selector: 'ui',
   styleUrls: ['./kiosko.scss'],
  templateUrl: './kiosko.html'
})
export class Ui {
  public admin= JSON.parse(localStorage.getItem('identity'));
	public kioskos : Kiosko[];
  public kioskosD : KioskoD[];
  public kiosko: Kiosko;
  public kioskoD: KioskoD;
	public activo: boolean;
	public edit: boolean;
	public page: number;
  public message: string;
  public n: number;
  public cantPage: number;
  public limit: number;
  
  constructor(
	  private modalService: NgbModal,
  	private _kioskoService: KioskoService,
    private _kioskoDService: KioskoDService,
    private _empresaService: EmpresaService,
    private _botonService: BotonService
  ){
	this.kioskos = new Array<Kiosko>();
  this.kioskosD = new Array<KioskoD>();
  this.kiosko= new Kiosko('','','',false,'','',0,JSON.parse(localStorage.getItem('identity')).empresa)
  this.kioskoD = new KioskoD('','','',0,false,'','');
	this.activo = true;
	this.listKiosko();
	this.edit= false;
  this.message='No hay kioskos creados';
	this.page=1;
  this.n=1;
  this.cantPage=0;
  this.limit=8;
  this.getEmpresa();
    setInterval(()=>{this.active()},15000)
  }
  	lgModalShowKiosko(kiosko){
      //visualizar
    	const activeModal = this.modalService.open(DefaultModal3, {size: 'lg'});
    	activeModal.componentInstance.kiosko = kiosko;
  		activeModal.componentInstance.modalHeader = kiosko.nombre+' - ID: '+kiosko.id;
  		activeModal.componentInstance.show = 'true';
  		
  	}
  	lgModalDeleteKiosko(kiosko){
      //visualizar
    	const activeModal = this.modalService.open(DefaultModal3, {size: 'lg'});
    	activeModal.componentInstance.kiosko = kiosko;
  		activeModal.componentInstance.modalHeader = 'Eliminar El Kiosko '+kiosko.nombre;
  		activeModal.componentInstance.show = 'delete';
  		
  	}
    lgModalDeleteKioskoD(kiosko){
      //visualizar
      const activeModal = this.modalService.open(DefaultModal3, {size: 'lg'});
      activeModal.componentInstance.kioskoD = kiosko;
      activeModal.componentInstance.modalHeader = 'Eliminar El Kiosko '+kiosko.descripcion;
      activeModal.componentInstance.show = 'deleteD';
      
    }
    makeid(){
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      for( var i=0; i < 7; i++ )
          text += possible.charAt(Math.floor(Math.random() * possible.length));

      this.kiosko.id=text;
      this.kioskoD.id=text;
    }
  	
	close(){
		this.edit = false;
    this.kiosko= new Kiosko('','','',false,'','',0,this.admin.empresa);
    this.createF=false;
    this.createD=false;
    this.editB=false;
    this.botons= null;
		this.listKiosko();
	}

  next(){
    if(this.n<this.cantPage)
      this.n+=1;
  }
  prev(){
    if(this.n!=1){
      this.n-=1;
    }
  }

  public tipeButon: string;
  public empresa;
  getEmpresa(){
    this._empresaService.getEmpresa(this.admin.empresa).subscribe(
      response=>{
        if(!response.empresa){
          this.tipeButon= 'multiple';
        }else{
          this.empresa= response.empresa;
          if(this.empresa.tipoK=='Multiple'){
            this.tipeButon='multiple';
          }else{
            this.tipeButon='otros'
          }

        }
      }
    )
  }
   	listKiosko(){
  		this._kioskoService.listKiosko(this.admin.empresa).subscribe(
  			response=>{
  				if(!response.kioskos){
  					alert(response);
  				}else{
  					this.kioskos=response.kioskos;
              
              this.cantPage = response.total/this.limit;
              if(this.kioskos.length>0)
                this.message='';
            
  				}
  			},
  			error=>{
  				var errorMessage = <any>error;
	          	if(errorMessage != null){
	            	//this.errorMessage = body.message;
	            	console.log(error);
  				}
  			}
  		)
      this._kioskoDService.listKiosko(this.admin.empresa).subscribe(
        response=>{
          if(!response.kioskos){
            alert(response.message)
          }else{
            this.kioskosD= response.kioskos;
            
            if(this.kioskosD.length>0)
              this.message='';
          }
        }
      )
  	}
  
    active(){
      for (var i=0;i<this.kioskos.length; ++i){
        this._kioskoService.getKiosko(this.kioskos[i]).subscribe(
          response=>{
            if(response.kiosko){
              var date = new Date();
              var h= date.getHours()*3600;
              var m= date.getMinutes()*60;
              var s= date.getSeconds();
                     
              var dif = (h+m+s)- response.kiosko.time;
              console.log(dif+' Kiosko:'+response.kiosko.nombre);
              if((dif>35)&&(response.kiosko.estado==true)){
                response.kiosko.estado=false;
                this._kioskoService.update2(response.kiosko).subscribe(
                  response=>{
                    if(!response.kiosko){
                      alert('Error al intentar listar los kioskos activos');
                    }
                  },
                  error=>{
                    var errorMessage = <any>error;
                      if(errorMessage != null){
                        //this.errorMessage = body.message;
                        console.log(error);
                      }
                  }
                );
              }
              if(dif<0){
                response.kiosko.estado=false;
                this._kioskoService.update2(response.kiosko).subscribe(
                  response=>{
                    if(!response.kiosko){
                      alert('Error al intentar listar los kioskos activos');
                    }
                  },
                  error=>{
                    var errorMessage = <any>error;
                      if(errorMessage != null){
                        //this.errorMessage = body.message;
                        console.log(error);
                      }
                  }
                );
              }
            
            }
          },
          error=>{
            var errorMessage = <any>error;
              if(errorMessage != null){
                //this.errorMessage = body.message;
                console.log(error);
            }
          }
        )
      }
      this.listKiosko();
    }

  public createF=false;
  public createD=false;
  public boton= 2;
  public crearForm(){
    if(this.createF){
      this.createF=false;
      this.createD=false;
    }else{
      this.editB=false;
      this.createF=true;
      this.createD=false;

      this.kiosko= new Kiosko('','','',false,'','',0,this.admin.empresa)
      this.makeid();
    }
  }
  public crearDir(){
    if(this.createD){
      this.createD=false;
      this.createF=false;
    }else{
      this.createD=true;
      this.editB=false;
      this.createF=false;
      this.boton=this.empresa.maxboton;
      this.kioskoD= new KioskoD('','','',0,false,'',this.admin.empresa)      
      this.makeid();
    }
  }
  crear(){
    if(this.empresa.tipoK=='Formulario'){
      this.crearForm();
    }else{
      this.crearDir();
    }
  }
  public editB=false;
  editBoton(kiosko){
    if(this.editB){
      this.editB=false;
      this.kioskoD= new KioskoD('','','',0,false,'',this.admin.empresa)      
    }else{
      this.createD=false;
      this.createF=false;
      this.editB=true;
      this.listBotones(kiosko);
    }
    
  }
  public botons: Boton;
  public listBotones(kiosko){
    this._botonService.listbotons(kiosko._id).subscribe(
      response=>{
        if(response.boton){
          this.botons=response.boton;
        }
      }
    )
  }
  public editKD(kiosko){
    if(this.createD){
      this.createD=false;
      this.createF=false;
    }else{
      this.kioskoD=kiosko;
      this.createD=true;
      this.createF=false;
      this.editB=false;
    }
  }
  public editar(kiosko){
    if(this.createF){
      this.createD=false;
      this.createF=false;
    }else{
      this.kiosko=kiosko;
      this.createF=true;
      this.createD=false;
      this.editB=false;
    }
  }
}

