import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import {Cuestionario} from '../../../models/cuestionario'
import {CuestionarioService} from '../../../services/cuestionario.services'
import {Kiosko} from '../../../models/kiosko'
import {KioskoD} from '../../../models/kioskoD'
import {KioskoService} from '../../../services/kiosko.services'
import {KioskoDService} from '../../../services/kioskod.services'
import {PreguntaService} from '../../../services/pregunta.services'
import {Pregunta} from '../../../models/pregunta'


declare let jsPDF;
@Component({
  selector: 'add-service-modal',
  styleUrls: [('./default-modal.component.scss')],
  templateUrl: './default-modal.component.html'
})

export class DefaultModal3 implements OnInit {
  
  modalHeader: string;
  modalContent: string;
  show: string;
  cuestionarios: Cuestionario[];
  public kiosko: Kiosko;
  public kioskoD: KioskoD;
  public preguntas: Pregunta[];
  constructor(
    private activeModal: NgbActiveModal,
    private _cuestionarioService: CuestionarioService,
    private _kioskoService: KioskoService,
    private _kioskoDService: KioskoDService,
    private _preguntaService: PreguntaService,
    ){
    this.kiosko = new Kiosko('','','',false,'','',0,'');
    this.preguntas = new Array<Pregunta>();
    this.listCuestionarios();
  }

  ngOnInit() {
    if(this.show == 'true'){
      this.listPregunta();

    }
  }

  closeModal() {
    this.activeModal.close();
    this.kiosko = new Kiosko('','','',false,'','',0,'');
    
  }
  makeid(){
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 7; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    this.kiosko.id=text;
  }
  save(){
    if(this.kiosko._id == ''){
      if((this.kiosko.nombre != '')&&(this.kiosko.id != '')){
        this._kioskoService.save(this.kiosko).subscribe(
          response=>{
            if(!response.kiosko){
               alert('No se pudo crear el kiosko');
            }else{
              this.activeModal.close();
              this.kiosko = new Kiosko('','','',false,'','',0,'');
              let win = (window as any);
              win.location.reload();
            }
          },
          error=>{
            var errorMessage = <any>error;
            if(errorMessage != null){
              //this.errorMessage = body.message;
              console.log(error);
            }
          }
        )
      }else{
        alert('Debe darle nombre al kiosko');
      }
  }else{
    if((this.kiosko.nombre != '')&&(this.kiosko.id != '')){
      this._kioskoService.update(this.kiosko).subscribe(
          response=>{
            if(!response.kiosko){
              alert(response.message);
            }else{
              this.closeModal();
            }
          },
          error=>{
            var errorMessage = <any>error;
            if(errorMessage != null){
              //this.errorMessage = body.message;
              console.log(error);
            }
          }
        )       
    }else{
      alert('Debe darle nombre al kiosko');
    }
  }
}
  listCuestionarios(){
    this._cuestionarioService.listCuestionario(JSON.parse(localStorage.getItem('identity')).empresa).subscribe(
        response =>{
          if(!response.cuestion){
            console.log(response);
          }else{
            this.cuestionarios = response.cuestion;
          }
            
          },
          error=>{
            var errorMessage = <any>error;
            if(errorMessage != null){
              //this.errorMessage = body.message;
              console.log(error);
             }
          }
      );
  };
  listPregunta(){
    this._preguntaService.listPreguntas(this.kiosko.cuestionario).subscribe(
        response =>{
          if(!response.pregunta){
            alert('No se pudieron cargar las preguntas');
          }else{
            this.preguntas = response.pregunta;
          }
        },
        error =>{
          var errorMessage = <any>error;
            if(errorMessage != null){
              console.log(error);
            }
        }
      );
  }
  deleteKiosko(){
      this._kioskoService.delete(this.kiosko._id).subscribe(
        response=>{
          if(!response.kiosko){
            alert(response);
          }else{
            let win = (window as any);
            win.location.reload();
          }
        },
        error=>{
          var errorMessage = <any>error;
              if(errorMessage != null){
                //this.errorMessage = body.message;
                console.log(error);
          }
        }
      )
    }
  deleteKioskoD(){
    this._kioskoDService.delete(this.kioskoD._id).subscribe(
      response=>{
        if(!response.kioskoD){
          alert(response);
        }else{
          let win = (window as any);
          win.location.reload();
        }
      },
      error=>{
        var errorMessage = <any>error;
          if(errorMessage != null){
            //this.errorMessage = body.message;
            console.log(error);
          }
      }
    )
  }
}
