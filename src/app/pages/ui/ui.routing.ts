import { Routes, RouterModule }  from '@angular/router';

import { Ui } from './ui.component';
import {AuthGuard} from '../../guards/index';
// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: Ui,
    children: [
    //  { path: 'buttons', component: Buttons },
    //  { path: 'grid', component: Grid },
    //  { path: 'icons', component: Icons },
    //  { path: 'typography', component: Typography },
    //  { path: 'modals', component: Modals }
    ],
    canActivate: [AuthGuard]
  }
];

export const routing = RouterModule.forChild(routes);
