import { Routes, RouterModule } from '@angular/router';

import { Tables } from './tables.component';

// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: Tables,
    children: [
     // { path: 'basictables', component: BasicTables },
     // { path: 'smarttables', component: SmartTables },
     // { path: 'datatables', component: DataTables },
     // { path: 'hottables', component: HotTablesComponent }
    ]
  }
];

export const routing = RouterModule.forChild(routes);
