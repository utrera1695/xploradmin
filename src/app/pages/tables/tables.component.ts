import {Component} from '@angular/core';
import {AdminService} from '../../services/admin.services'
import {Admin} from '../../models/admin'
import {EmpresaService} from '../../services/empresa.services'
import {Empresa} from '../../models/empresa'
import {MailService} from '../../services/mail.services'

@Component({
  selector: 'tables',
  templateUrl: 'admin.html',
  styleUrls:['./admin.scss']
})
export class Tables {

  public admin: Admin;
  public empresa: Empresa;
  public name2: string;
  public email2: string;
  public pass: string;
  public passconfirm: string;
  public showUpP: boolean;
  public showUpE: boolean;
  public message: string;
  constructor(
  	private _adminService: AdminService,
    private _empresaService: EmpresaService,
    private _mailService: MailService
  ){
  	this.admin= this._adminService.getIdentity();
    this.empresa=new Empresa('','','',0,'')
  	this.name2='';
  	this.email2='';
  	this.pass='';
  	this.passconfirm='';
  	this.message='';
  	this.showUpE=this.showUpP=false;
    this.getEmpresa();
  }
    public getEmpresa(){
      this._empresaService.getEmpresa(this.admin.empresa).subscribe(
       response=>{
         if(response.empresa){
           this.empresa=response.empresa
         }
       }
      )
    }
  	public update2(){
  		this.erorrpass=false;
  		if((this.pass==this.passconfirm)&&(this.pass!='')){      
    		this.admin.pass=this.pass;
    		this.admin.password=this.pass;
            this._adminService.update(this.admin).subscribe(
              response =>{
                let admin = response.adminU;
                
                if(!admin._id){
                  //mensaje de error
                  this.message =response.message;
                }else{
                  this.admin = admin;
                  //mensaje de correcto
                  localStorage.setItem('identity',JSON.stringify(admin));
                  this.showUpP=false;
                  this.message=' Se han actualizado sus datos sin problema';
                  this._mailService.sendMail2(this.admin).subscribe(
                    response=>{
                      if(response.info){
                        setTimeout(()=>{this.message=''},3000);
                        let win = (window as any);
                        win.location.reload();
                      }
                    }
                  )
                  
	            }
              },
              error=>{
                var errorMessage = <any>error;
                if(errorMessage != null){
                  //this.errorMessage = body.message;
                  console.log(error);
                }
              }
            );
        }else{
        	this.erorrpass=true;
        	this.message='La contraseña no coincide';
        }
  	}
    public update(){
    this.erroremail=false;
    if(this.notNull()){
        if(this.email2.search('@')>4){
            if(this.validateCharacter()){
            		this.admin.name=this.name2;
            		if((this.email2!=this.admin.email)&&(this.email2!='')){
	            		this._adminService.getAdminEmail(this.email2).subscribe(
	                    response=>{
	                    if(!response.admin){
		                    this.admin.email=this.email2;
		                    
			                    this._adminService.update(this.admin).subscribe(
			                      response =>{
			                        let admin = response.adminU;
			                        
			                        if(!admin._id){
			                          //mensaje de error
			                          this.message =response.message;
			                        }else{
			                          localStorage.setItem('identity',JSON.stringify(this.admin));
			                          this.admin=this._adminService.getIdentity();
			                          //mensaje de correcto
			                          this.showUpE=false;
			                          this.message=' Se han actualizado sus datos sin problema';
			                          setTimeout(()=>{this.message=''},3000);
						            }
			                      },
			                      error=>{
			                        var errorMessage = <any>error;
			                        if(errorMessage != null){
			                          //this.errorMessage = body.message;
			                          console.log(error);
			                        }
			                      }
			                    );
    			            }else{
    			            	this.erroremail=true;
    			            	this.message='Esta cuenta de correo electrónico ya le pertenece a otro usuario';
    			            }
	                    })
	                }else{
	                	this._adminService.update(this.admin).subscribe(
	                      response =>{
	                        let admin = response.adminU;
	                        
	                        if(!admin._id){
	                          //mensaje de error
	                          this.message =response.message;
	                        }else{
	                          localStorage.setItem('identity',JSON.stringify(this.admin));
	                          this.admin=this._adminService.getIdentity();
	                          //mensaje de correcto
	                          this.showUpE=false;
	                          this.message=' Se han actualizado sus datos sin problema';
				              setTimeout(()=>{this.message=''},3000);
						            
				            }
	                      },
	                      error=>{
	                        var errorMessage = <any>error;
	                        if(errorMessage != null){
	                          //this.errorMessage = body.message;
	                          console.log(error);
	                        }
	                      }
	                    );
	                }
                
            }
        }else{
        	this.erroremail=true;
            this.message='Por favor introduzca una direcíón de correo electrónico valido';
        }
    }else{
    	this.errorname=true;
      this.message= 'Debe rellenar algunos campos obligatorios antes de guardar';
    }
  }
  notNull(){
    if((this.name2!='')){
      this.errorname=false;
      return true;
    }
    this.errorname=true;
    return false;
  }
  validateCharacter(){
    if(this.name2.length>=4){
      this.errorname=false;
      return true;
    }else{
      this.errorname=true;
      this.message='Su nombre y apellido debe de ser mayor a cuatro caracteres';
      return false;
    }
  }

  validateP(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode( key );
    var regex = /[0-9]|[a-z]|[A-Z]/;
    if( !regex.test(key) ) {
      theEvent.returnValue = false;
      if(theEvent.preventDefault) theEvent.preventDefault();
    }
  }
  validateN(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode( key );
    var regex = /[a-záéíóúñ]|[A-ZÁÉÍÓÚÑ]|\ /;
  	if( !regex.test(key) ) {
      theEvent.returnValue = false;
      if(theEvent.preventDefault) theEvent.preventDefault();
    }
  }
  public erorrpass=false;
  passError(){
    var style={
      'border-color': this.erorrpass ? '#FFC107':'#3c444a'
    }
    return style;
  }
  public erroremail=false;
  emailError(){
    var style={
      'border-color': this.erroremail ? '#FFC107':'#3c444a'
    }
    return style;
  }
  public errorname=false;
  
  nameError(){
    var style={
      'border-color': this.errorname ? '#FFC107':'#3c444a'
    }
    return style;
  }
}
