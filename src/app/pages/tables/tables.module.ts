import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';
import { HttpModule } from "@angular/http";

import { routing } from './tables.routing';
import { Tables } from './tables.component';
import {AdminService} from '../../services/admin.services'
import {EmpresaService} from '../../services/empresa.services'
import {MailService} from '../../services/mail.services'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgaModule,
    routing,
    HttpModule,
  ],
  declarations: [
    Tables,

  ],
  providers: [
      AdminService,
      EmpresaService,
      MailService
  ]
})
export class TablesModule {
}
