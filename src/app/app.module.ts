import { BrowserModule } from '@angular/platform-browser';
import { NgModule , ApplicationRef } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule,JsonpModule} from '@angular/http';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { AppTranslationModule } from './app.translation.module';
import { CommonModule }  from '@angular/common';

/*
 * Platform and Environment providers/directives/pipes
 */
import { routing } from './app.routing';
import {AuthGuard} from './guards/index';
import { AppState, InternalStateType } from './app.service';
import { GlobalState } from './global.state';
import { NgaModule } from './theme/nga.module';
import { PagesModule } from './pages/pages.module';

import { AppComponent } from './app.component';
import { AdminService } from './services/admin.services'



// Application wide providers
const APP_PROVIDERS = [
  AppState,
  GlobalState
];

export type StoreType = {
  state: InternalStateType,
  restoreInputValues: () => void,
  disposeOldHosts: () => void
};


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AppTranslationModule,
    FormsModule,
    HttpModule,
    JsonpModule,
    RouterModule,
    ReactiveFormsModule,
    NgaModule.forRoot(),
    NgbModule.forRoot(),
    PagesModule,
    routing
  ],
  providers: [
    APP_PROVIDERS,
    AuthGuard,
    AdminService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(
    public appState: AppState
  ){}
}
