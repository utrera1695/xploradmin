export class Slide{
	constructor(
		public _id: string,
		public descripcion: string,
		public empresa: string,
		public imagenes: string[]
	){}
}
