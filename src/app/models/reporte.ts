export class Reporte{
	constructor(
		public _id: string,
		public cuestionario: string,
		public rpregunta1: string,
		public rpregunta2: string,
		public rpregunta3: string,
		public rpregunta4: string,
		public rpregunta5: string,
		public user: string,
	){}
}