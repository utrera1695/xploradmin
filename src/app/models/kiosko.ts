export class Kiosko{
	constructor(
		public _id: string,
		public id: string,
		public nombre: string,
		public estado: boolean,
		public cuestionario: string,
		public tema: string,
		public time: number,
		public empresa: string
	){}
}