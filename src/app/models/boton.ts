export class Boton{
	constructor(
		public _id: string,
		public descripcion: string,
		public estado: boolean,
		public enlace: string,
		public icono: string,
		public kiosko: string
	){}
}