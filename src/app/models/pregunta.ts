export class Pregunta{
	constructor(
		public _id: string,
		public nombre: string,
		public tipo: string,
		public cuestionario: string
		){}
}