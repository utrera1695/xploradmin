export class User{
	constructor(
		public _id: string,
		public nombre: string,
		public apellido: string,
		public edad: string,
		public sexo: string,
		public pais: string,
		public ciudad: string,
		public telefono: string,
		public correo: string,
		public cuestionario: string,
	){

	}
}