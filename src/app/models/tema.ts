export class Tema{
	constructor(
		public _id: string,
		public nombre: string,
		public archivo: string,
		public fraseP: string,
		public fraseF: string,
		public empresa: string
		){}
}