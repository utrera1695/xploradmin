export class KioskoD{
	constructor(
		public _id: string,
		public descripcion: string,
		public id : string,
		public time: number,
		public estado: boolean,
		public slideshow: string,
		public empresa: string
	){}
}
