export class ReporteC{
	constructor(
		public _id: string,
		public cuestionario: string,
		public cuestionarioN: string,
		public pregunta1: string,
		public tipo1: string,
		public pregunta2: string,
		public tipo2: string,
		public pregunta3: string,
		public tipo3: string,
		public pregunta4: string,
		public tipo4: string,
		public pregunta5: string,
		public tipo5: string,
		public empresa: string
	){}
}