import {Injectable} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {GLOBAL} from './global';

@Injectable()
export class KioskoDService{
	
	public url: string;
	constructor(private _http: Http){
		this.url = GLOBAL.url;
	}
	save(kiosko_to_save){
		let params = JSON.stringify(kiosko_to_save);	
		let headers = new Headers({'Content-Type':'application/json'});

		return this._http.post(this.url+'kioskod',params,{headers: headers}).map(res=>res.json());	
	}
	listKiosko(empresa/*token,admin.id*/){
		let headers = new Headers({'Content-Type':'application/json'});

		return this._http.get(this.url+'kioskosD/'+empresa).map(res => res.json());
	}
	listKiosko2(empresa,page){
		return this._http.get(this.url+'kioskosD/'+empresa+'/'+page).map(res => res.json());	
	}
	delete(kiosko_to_delete){
		return this._http.delete(this.url+'kioskoD/'+kiosko_to_delete).map(res=>res.json());
	}
	update(kiosko_to_update){
		let params = JSON.stringify(kiosko_to_update);	
		let headers = new Headers({'Content-Type':'application/json'});

		return this._http.put(this.url+'kioskoD/'+kiosko_to_update._id,params,{headers: headers}).map(res=>res.json());		
	}
	update2(kiosko_to_update){
		let params = JSON.stringify(kiosko_to_update);	
		let headers = new Headers({'Content-Type':'application/json'});

		return this._http.put(this.url+'kiosko2/'+kiosko_to_update._id,params,{headers: headers}).map(res=>res.json());		
	}
	getKiosko(kiosko){
		return this._http.get(this.url+'kioskoD/'+kiosko._id).map(res=>res.json());
	}
}