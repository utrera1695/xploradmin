import {Injectable} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {GLOBAL} from './global';

@Injectable()
export class MailService{
	
	public url: string;
	constructor(private _http: Http){
		this.url = GLOBAL.url;
	}
	sendMail(admin){
		return this._http.post(this.url+'sendmail',admin).map(res=>res.json());
	}
	sendMail2(admin){
		return this._http.post(this.url+'sendmail2',admin).map(res=>res.json());
	}
}
