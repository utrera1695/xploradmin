import {Injectable} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {GLOBAL} from './global';

@Injectable()
export class ReporteCService{
	
	public url: string;
	constructor(private _http: Http){
		this.url = GLOBAL.url;
	}


	listReporteC(empresa/*token,admin.id*/){
		let headers = new Headers({'Content-Type':'application/json'});

		return this._http.get(this.url+'listReportec/'+empresa).map(res => res.json());
	}
	listReporteC2(page,empresa/*token,admin.id*/){
		let headers = new Headers({'Content-Type':'application/json'});

		return this._http.get(this.url+'listReportec/'+page+'/'+empresa).map(res => res.json());
	}

	delete(reporte_to_delete){
		//let headers = new Headers({'Content-Type':'application/json'});
		return this._http.delete(this.url+'delete/'+reporte_to_delete).map(res=>res.json());
	}
	get(reporteId){
		return this._http.get(this.url+'reportec/'+reporteId).map(res=>res.json())
	}
}