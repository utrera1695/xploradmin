import {Injectable} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {GLOBAL} from './global';

@Injectable()
export class ReporteService{
	
	public url: string;
	constructor(private _http: Http){
		this.url = GLOBAL.url;
	}

	save(reporte_to_save){
		let params = JSON.stringify(reporte_to_save);	
		let headers = new Headers({'Content-Type':'application/json'});

		return this._http.post(this.url+'reporte',params,{headers: headers}).map(res=>res.json());
	}
	listReport(cuestionarioId){
		return this._http.get(this.url+'listReportes/'+cuestionarioId).map(res=>res.json());
	}
	listAllReport(){
		return this._http.get(this.url+'listAll').map(res=>res.json());
	}
}