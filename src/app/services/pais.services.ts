import {Injectable} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {GLOBAL} from './global';

@Injectable()
export class PaisService{
	
	public url: string;
	constructor(private _http: Http){
		this.url = GLOBAL.url;
	}

	save(pais_to_save){
		let params = JSON.stringify(pais_to_save);	
		let headers = new Headers({'Content-Type':'application/json'});

		return this._http.post(this.url+'savePais',params,{headers: headers}).map(res=>res.json());
	
	}
}