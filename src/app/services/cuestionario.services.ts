import {Injectable} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {GLOBAL} from './global';

@Injectable()
export class CuestionarioService{
	
	public url: string;
	constructor(private _http: Http){
		this.url = GLOBAL.url;
	}

	save(cuestionario_to_save){
		let params = JSON.stringify(cuestionario_to_save);	
		let headers = new Headers({'Content-Type':'application/json'});

		return this._http.post(this.url+'cuestionario',params,{headers: headers}).map(res=>res.json());
	
	}

	listCuestionario(empresa/*token,admin.id*/){
		let headers = new Headers({'Content-Type':'application/json'});

		return this._http.get(this.url+'listeCuestionario/'+empresa).map(res => res.json());
	}
	
	delete(cuestionario_to_delete){
		//let headers = new Headers({'Content-Type':'application/json'});
		return this._http.delete(this.url+'deletCuestionario/'+cuestionario_to_delete).map(res=>res.json());
	}
	get(cuestionarioId){
		return this._http.get(this.url+'cuestionario/'+cuestionarioId).map(res => res.json());
	}
	update(cuestionario){
		let params = JSON.stringify(cuestionario);	
		let headers = new Headers({'Content-Type':'application/json'});

		return this._http.put(this.url+'cuestionario/'+cuestionario._id,params,{headers: headers}).map(res=>res.json());	
	}
}