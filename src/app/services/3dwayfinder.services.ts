import { Injectable } from '@angular/core';
import { Http, Response,Jsonp } from '@angular/http';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class Wayfinder3d{
	
	public url='http://api.3dwayfinder.com/admin/';
	public identity;
	public token;
	public invocation = new XMLHttpRequest();
	

	constructor(
		private _http: Http,
		private _jsonp: Jsonp){

	}
	

	listZone(){
		var url='http://api.3dwayfinder.com/admin/21da6fd69ff4ccbc9d4441d521fef9a0/editpoigroups/getPOIGroups/en'
		this._jsonp.request(url, { method: 'Get' }).map(res => res => res.json())
	}	
}
