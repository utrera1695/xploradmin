import {Injectable} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {GLOBAL} from './global';

@Injectable()
export class EmpresaService{
	
	public url: string;
	constructor(private _http: Http){
		this.url = GLOBAL.url;
	}

	save(empresa_to_save){
		let params = JSON.stringify(empresa_to_save);	
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.post(this.url+'empresa',params,{headers: headers}).map(res=>res.json());
	}

	listEmpresa(/*token,admin.id*/){
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.get(this.url+'empresas').map(res => res.json());
	}
	delete(empresa_to_delete){
		//let headers = new Headers({'Content-Type':'application/json'});
		return this._http.delete(this.url+'empresadelete/'+empresa_to_delete._id).map(res=>res.json());
	}
	getEmpresa(empresa){
		return this._http.get(this.url+'empresa/'+empresa).map(res => res.json());
	}
	getEmpresaname(empresaname){
		return this._http.get(this.url+'enpresaname/'+empresaname).map(res=>res.json());
	}
	update(empresa){
		let params = JSON.stringify(empresa);	
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.put(this.url+'empresaupdate/'+empresa._id,params,{headers: headers}).map(res=>res.json());	
	}
}