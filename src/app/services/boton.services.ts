import {Injectable} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {GLOBAL} from './global';

@Injectable()
export class BotonService{
	
	public url: string;
	constructor(private _http: Http){
		this.url = GLOBAL.url;
	}

	save(boton_to_save){
		let params = JSON.stringify(boton_to_save);	
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.post(this.url+'boton',params,{headers: headers}).map(res=>res.json());
	}

	listbotons(kioskoId/*token,admin.id*/){
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.get(this.url+'botons/'+kioskoId).map(res => res.json());
	}
	
	getEmpresa(empresa){
		return this._http.get(this.url+'empresa/'+empresa).map(res => res.json());
	}
	update(boton){
		let params = JSON.stringify(boton);	
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.put(this.url+'boton/'+boton._id,params,{headers: headers}).map(res=>res.json());	
	}
}