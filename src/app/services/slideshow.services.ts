import {Injectable} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {GLOBAL} from './global';

@Injectable()
export class SlideShowService{
	
	public url: string;
	constructor(private _http: Http){
		this.url = GLOBAL.url;
	}
	saveTema(slide_to_save){
		let params = JSON.stringify(slide_to_save);	
		let headers = new Headers({'Content-Type':'application/json'});

		return this._http.post(this.url+'slideshow',params,{headers: headers}).map(res=>res.json());

	}
	deleteSlide(sliderId){
		return this._http.delete(this.url+'slider/'+sliderId).map(res=>res.json());
	}
	getSlide(slider){
		return this._http.get(this.url+'slideshow/'+slider).map(res=>res.json());

	}
	makeFileRequest(url: string, params: Array<string>, files: Array<File>,name: string){
		return new Promise(function(resolve,reject){
			var formData:any = new FormData();
			var xhr = new XMLHttpRequest();
			for (var i=0; i<files.length;i++){
				formData.append(name,files[i],files[i].name);
			}
			xhr.onreadystatechange= function(){
				if(xhr.readyState == 4){
					if(xhr.status ==200){
						resolve(JSON.parse(xhr.response));
					}else{
						reject(xhr.response);
					}
				}
			}
			xhr.open('POST',url,true);
			//xhr.setRequestHeader('Authorization',token);
			xhr.send(formData);
		})
	}
	listSlide(empresa){
		let headers = new Headers({'Content-Type':'application/json'});
		if(empresa!=''){
			return this._http.get(this.url+'sliders/'+empresa).map(res => res.json());
		}else{
			return this._http.get(this.url+'sliders').map(res => res.json());
		}
	}
	updateSlide(slide){
		let params = JSON.stringify(slide);	
		let headers = new Headers({'Content-Type':'application/json'});

		return this._http.put(this.url+'slider/'+slide._id,params,{headers: headers}).map(res=>res.json());
	}
}