import {Injectable} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {GLOBAL} from './global';

@Injectable()
export class UserService{
	
	public url: string;
	constructor(private _http: Http){
		this.url = GLOBAL.url;
	}

	save(user_to_save){
		let params = JSON.stringify(user_to_save);	
		let headers = new Headers({'Content-Type':'application/json'});

		return this._http.post(this.url+'save',params,{headers: headers}).map(res=>res.json());
	
	}
	list(cuestionarioId){
		return this._http.get(this.url+'listUsers/'+cuestionarioId).map(res=>res.json());
	}
	
	list2(){
		return this._http.get(this.url+'listUsers').map(res=>res.json());	
	}
}