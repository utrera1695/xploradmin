import {Injectable} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {GLOBAL} from './global';

@Injectable()
export class TemaService{
	
	public url: string;
	constructor(private _http: Http){
		this.url = GLOBAL.url;
	}
	saveTema(tema_to_save){
		let params = JSON.stringify(tema_to_save);	
		let headers = new Headers({'Content-Type':'application/json'});

		return this._http.post(this.url+'savetema',params,{headers: headers}).map(res=>res.json());

	}
	makeFileRequest(url: string, params: Array<string>, files: Array<File>,name: string){
		console.log(url);
		return new Promise(function(resolve,reject){
			var formData:any = new FormData();
			var xhr = new XMLHttpRequest();
			for (var i=0; i<files.length;i++){
				formData.append(name,files[i],files[i].name);
			}
			xhr.onreadystatechange= function(){
				if(xhr.readyState == 4){
					if(xhr.status ==200){
						resolve(JSON.parse(xhr.response));
					}else{
						reject(xhr.response);
					}
				}
			}
			xhr.open('POST',url,true);
			//xhr.setRequestHeader('Authorization',token);
			xhr.send(formData);
		})
	}
	deleteTema(temaId){
		return this._http.delete(this.url+'deleteTema/'+temaId).map(res=>res.json());
	}
	getTema(temaId){
		let headers = new Headers({'Content-Type':'application/json'});

		return this._http.get(this.url+'getTema/'+temaId).map(res => res.json());
	}
	listTema(empresa){
		let headers = new Headers({'Content-Type':'application/json'});

		return this._http.get(this.url+'listTemas/'+empresa).map(res => res.json());
	}
	update(tema){
		let params = JSON.stringify(tema);	
		let headers = new Headers({'Content-Type':'application/json'});

		return this._http.put(this.url+'updatetema/'+tema._id,params,{headers: headers}).map(res=>res.json());
	}
}