import {Injectable} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {GLOBAL} from './global';

@Injectable()
export class OpcionesService{
	
	public url: string;
	constructor(private _http: Http){
		this.url = GLOBAL.url;
	}

	save(opciones_to_save){
		let params = JSON.stringify(opciones_to_save);	
		let headers = new Headers({'Content-Type':'application/json'});
		console.log(opciones_to_save);
		return this._http.post(this.url+'opciones',params,{headers: headers}).map(res=>res.json());
	
	}

	getOpciones(preguntaId){
		let headers = new Headers({'Content-Type':'application/json'});

		return this._http.get(this.url+'opciones/'+preguntaId).map(res => res.json());
	}
	update(opcion){
		let params = JSON.stringify(opcion);	
		let headers = new Headers({'Content-Type':'application/json'});

		return this._http.put(this.url+'opciones/'+opcion._id,params,{headers: headers}).map(res=>res.json());

	}
}