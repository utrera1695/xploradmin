import {Injectable} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {GLOBAL} from './global';

@Injectable()
export class AdminService{
	
	public url: string;
	public identity;
	public token;

	constructor(private _http: Http){
		this.url = GLOBAL.url;
	}
	listAdmins(){
		return this._http.get(this.url+'admins').map(res => res.json());
	}
	getAdmin(admin){
		return this._http.get(this.url+'admind/'+admin).map(res=>res.json());
	}
	getAdminEmail(email){
		return this._http.get(this.url+'adminemail/'+email).map(res=>res.json());
	}

	signup(admin_to_login, gethash = null){
		if(gethash !=null){
			admin_to_login.gethash = gethash;
		}

		let json = JSON.stringify(admin_to_login);
		let params = json;

		let headers = new Headers({'Content-Type':'application/json'});

		return this._http.post(this.url+'login',params,{headers: headers}).map(res=>res.json());
	}

	register(admin_to_register){
		let params = JSON.stringify(admin_to_register);	
		let headers = new Headers({'Content-Type':'application/json'});
		console.log(admin_to_register);
		return this._http.post(this.url+'addAdmin',params,{headers: headers}).map(res=>res.json());
	
	}

	getIdentity(){
		let identity =JSON.parse(localStorage.getItem('identity'));
		if(identity != 'undefined'){
			this.identity = identity;
		}else{
			this.identity= null;
		}
		return this.identity;
	}

	getToken(){
		let token = localStorage.getItem('token');
		if(token != 'undefined'){
			this.token = token;
		}else{
			this.token = null;
		}
		return this.token;
	}

	delete(admin_to_delete){
		return this._http.delete(this.url+'admin/'+admin_to_delete).map(res=>res.json());
	
	}
	update(admin_to_update){
		let params = JSON.stringify(admin_to_update);	
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.put(this.url+'admin/'+admin_to_update._id,params,{headers: headers}).map(res=>res.json());
	}
	
}