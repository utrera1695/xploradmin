import {Injectable} from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import 'rxjs/add/operator/map';
import {Observable} from 'rxjs/Observable';
import {GLOBAL} from './global';

@Injectable()
export class PreguntaService{
	
	public url: string;
	constructor(private _http: Http){
		this.url = GLOBAL.url;
	}

	save(pregunta_to_save){
		let params = JSON.stringify(pregunta_to_save);	
		let headers = new Headers({'Content-Type':'application/json'});

		return this._http.post(this.url+'pregunta',params,{headers: headers}).map(res=>res.json());
	
	}

	listPreguntas(cuestionarioID){
		let headers = new Headers({'Content-Type':'application/json'});
		return this._http.get(this.url+'preguntas/'+cuestionarioID).map(res => res.json());
	}

	listPreguntas2(empresa){
		let headers = new Headers({'Content-Type':'application/json'});
		if(!empresa)
			return this._http.get(this.url+'preguntas').map(res => res.json());
		else
			return this._http.get(this.url+'preguntas/'+empresa).map(res => res.json());
	}

	delete(pregunta){
		return this._http.delete(this.url+'pregunta/'+pregunta).map(res => res.json());
	}
	update(pregunta){
		let params = JSON.stringify(pregunta);	
		let headers = new Headers({'Content-Type':'application/json'});

		return this._http.put(this.url+'preguntaupdate/'+pregunta._id,params,{headers: headers}).map(res=>res.json());

	}
}