import { Component, ViewContainerRef, OnInit } from '@angular/core';
import * as $ from 'jquery';
import { GlobalState } from './global.state';
import { BaImageLoaderService, BaThemePreloader, BaThemeSpinner } from './theme/services';
import { BaThemeConfig } from './theme/theme.config';
import { layoutPaths } from './theme/theme.constants';
import {AdminService} from './services/admin.services'
import {Admin} from './models/admin'


@Component({
  selector: 'app',
  styleUrls: ['./app.component.scss'],
  template: `
    <main [class.menu-collapsed]="isMenuCollapsed" baThemeRun>
      <div class="additional-bg"></div>
      <router-outlet></router-outlet>
    </main>
  `
})

export class AppComponent  implements OnInit{
  isMenuCollapsed: boolean = false;
  public user: Admin;
  constructor(
	  	
	  	private _state: GlobalState,
		private _imageLoader: BaImageLoaderService,
		private _spinner: BaThemeSpinner,
		private viewContainerRef: ViewContainerRef,
		private themeConfig: BaThemeConfig,
	 	private _adminService: AdminService	
  ){

	  themeConfig.config();
		this._loadImages();
	    this._state.subscribe('menu.isCollapsed', (isCollapsed) => {
	      	this.isMenuCollapsed = isCollapsed;	
	    });

  }
  public ngOnInit(){
    this.getAdmin();
  }
  public ngAfterViewInit(): void {
    // hide spinner once all loaders are completed
    BaThemePreloader.load().then((values) => {
      this._spinner.hide();
    });
  }

  private _loadImages(): void {
    // register some loaders
    BaThemePreloader.registerLoader(this._imageLoader.load('/assets/img/transblue-bg.jpg'));
  }
  public getAdmin(){
    if(this.user= JSON.parse(localStorage.getItem('identity'))){
      this._adminService.getAdmin(this.user.username).subscribe(
        response=>{
          if(!response.admin){
            localStorage.removeItem('identity');
            localStorage.removeItem('token');
            localStorage.clear();
          }else{
             localStorage.setItem('identity',JSON.stringify(response.admin));
          
          }
        }
      )
    }
  }
}
