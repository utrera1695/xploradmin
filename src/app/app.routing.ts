import {ModuleWithProviders} from '@angular/core';
import {Routes, RouterModule} from '@angular/router'


const routes : Routes =[
	//LISTA DE RUTAS CREADAS
	{ path: '', redirectTo: 'pages', pathMatch: 'full'},
    //{ path: '**', redirectTo: 'pages/dashboard'},
   
    //{path: '', redirectTo: 'pages/login'},
    {path: '**',redirectTo:''}

];

export const appRoutingProviders: any[] =[];
export const routing: ModuleWithProviders = RouterModule.forRoot(routes, { useHash: true });
