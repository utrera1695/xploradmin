import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';


@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private router: Router) {}

  canActivate() {
    if (localStorage.getItem('identity')) {
      return true;
    }

    this.router.navigate(['/login']);
    return false;
  }
}

/*public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
		this._adminService.getIdentity())){
			//SI ESTA LOGEADO EL USUARIO RETORNA tRUE
			console.log(localStorage.identity);
			return true;
		}
		//SI NO ESTA LOGUEADO RETORNA FALSE
		this.router.navigate(['/login']),{queryParams: {returnUrl: state.url}};
		console.log("no se ha registrado");
		return false;}	*/